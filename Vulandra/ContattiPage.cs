﻿using CoreGraphics;
using Foundation;
using GlobalToast;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Net.Mail;
using UIKit;

namespace Vulandra
{
    public partial class ContattiPage : UIViewController
    {
        GlobalStructureInstance GbsInstance = GlobalStructureInstance.Instance;

        UIColor Rosso = UIColor.FromRGB(220, 12, 33);
        UIMenuView menu;

        bool flagNome = false;
        bool flagEmail = false;
        bool flagMessaggio = false;

        public static ContattiPage Instance { private set; get; }

        public ContattiPage(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidAppear(bool animated)
        {
            try
            {
                GbsInstance.SetPage(PageName.Contatti);
            }
            catch (Exception) { };

            base.ViewDidAppear(animated);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            GbsInstance.SetPage(PageName.Contatti);

            ContattiPage.Instance = this;

            UIView NavigationBar = new UIView(new CGRect(0, 0, View.Frame.Width, 70));
            NavigationBar.BackgroundColor = UIColor.FromRGB(102, 104, 162);

            UIImageView MenuImage = new UIImageView(new CGRect(15, 27.5, 35, 35));
            MenuImage.Image = UIImage.FromFile("MenuClose.png");
            var imTap = new UITapGestureRecognizer(() => {
                if (menu.MenuOpen)
                {
                    menu.CloseMenu();
                }
                else
                {
                    menu.OpenMenu();
                }
            });
            MenuImage.UserInteractionEnabled = true;
            MenuImage.AddGestureRecognizer(imTap);
            NavigationBar.Add(MenuImage);

            UIImageView BackImage = new UIImageView(new CGRect(View.Frame.Width - 50, 27.5, 35, 35));
            BackImage.Image = UIImage.FromFile("Back.png");
            var backTap = new UITapGestureRecognizer(() => {
                this.NavigationController.PopToRootViewController(true);
            });
            BackImage.UserInteractionEnabled = true;
            BackImage.AddGestureRecognizer(backTap);
            NavigationBar.Add(BackImage);

            UIImageView LogoImage = new UIImageView(new CGRect(View.Frame.Width / 2 - 52.5, 27.5, 105, 35));
            LogoImage.Image = UIImage.FromFile("Logo.png");
            NavigationBar.Add(LogoImage);

            UIView Sep = new UIView(new CGRect(0, 68, View.Frame.Width, 2));
            Sep.BackgroundColor = Rosso;
            NavigationBar.Add(Sep);

            UIView TitleView = new UIView(new CGRect(0, 70, View.Frame.Width, 60));
			TitleView.BackgroundColor = Rosso;

            string Titletxt = NSBundle.MainBundle.LocalizedString("Contatti_Tit", "", null);
            var TitleSize = UIStringDrawing.StringSize(Titletxt, UIFont.FromName("OpenSans-Bold", 18), new CGSize(View.Frame.Width - 30, 80));

            UILabel TitleLabel = new UILabel(new CGRect(15, TitleView.Frame.Height / 2 - TitleSize.Height / 2, View.Frame.Width - 30, TitleSize.Height));
            TitleLabel.TextColor = UIColor.White;
            TitleLabel.TextAlignment = UITextAlignment.Center;
            TitleLabel.Font = UIFont.FromName("OpenSans-Bold", 18);
            TitleLabel.Lines = 0;
            TitleLabel.Text = Titletxt;
            TitleView.Add(TitleLabel);

            UIView SepTitle = new UIView(new CGRect(0, 58, View.Frame.Width, 2));
            SepTitle.BackgroundColor = Rosso;
            TitleView.Add(SepTitle);

            UIView MainView = new UIView(new CGRect(0, 130, View.Frame.Width, View.Frame.Height - 130));
            MainView.BackgroundColor = UIColor.White;

            UIScrollView scrollView = new UIScrollView(new CGRect(0, 0, MainView.Frame.Width, MainView.Frame.Height));
            scrollView.Bounces = false;
            MainView.Add(scrollView);

            int Y = 0;
            UIFont DataFont = UIFont.FromName("OpenSans-Bold", 16);

            string Hometxt = NSBundle.MainBundle.LocalizedString("Contatti_Home", "", null);
            UIImage HomeImage = UIImage.FromFile("Contatti_Home.png");
            var HomeView = CreateContactDataView( HomeImage, Hometxt, DataFont, new CGPoint(15,Y+15), (float)MainView.Frame.Width - 30);
            scrollView.Add(HomeView);

            Y += (int)HomeView.Frame.Height + 15;

            string Addresstxt = NSBundle.MainBundle.LocalizedString("Contatti_Indirizzo", "", null);
            UIImage AddressImage = UIImage.FromFile("Contatti_Indirizzo.png");
            var AddressView = CreateContactDataView(AddressImage, Addresstxt, DataFont, new CGPoint(15, Y + 15), (float)MainView.Frame.Width - 30);
            scrollView.Add(AddressView);

            Y += (int)AddressView.Frame.Height + 15;

            UIView RaggiungiciButton = new UIView();
            RaggiungiciButton.BackgroundColor = Rosso;
            RaggiungiciButton.Layer.CornerRadius = 5;
            UITapGestureRecognizer ButtonTap = new UITapGestureRecognizer(() => {
                View.EndEditing(true);

                string url = "http://maps.apple.com/?daddr=44.795707,11.974674";
                url = url.Replace(" ", "%20");
                if (UIApplication.SharedApplication.CanOpenUrl(new NSUrl(url)))
                {
                    UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
                }
                else
                {
                    new UIAlertView("Error", "Maps is not supported on this device", null, "Ok").Show();
                }

            });
            RaggiungiciButton.AddGestureRecognizer(ButtonTap);
            RaggiungiciButton.UserInteractionEnabled = true;
            scrollView.Add(RaggiungiciButton);

            UIImageView RaggiungiciButtonImage = new UIImageView(new CGRect(5, 5, 20, 20));
            RaggiungiciButtonImage.Image = UIImage.FromFile("Contatti_Raggiungici.png");
            RaggiungiciButton.Add(RaggiungiciButtonImage);


            string RaggiungiciButtontxt = NSBundle.MainBundle.LocalizedString("Contatti_Naviga", "", null);
            var RaggiungiciButtonSize = UIStringDrawing.StringSize(RaggiungiciButtontxt, UIFont.FromName("OpenSans", 10), new CGSize(200, 20));

            UILabel RaggiungiciButtonLabel = new UILabel(new CGRect(30, 15 - RaggiungiciButtonSize.Height / 2, RaggiungiciButtonSize.Width, RaggiungiciButtonSize.Height));
            RaggiungiciButtonLabel.TextColor = UIColor.White;
            RaggiungiciButtonLabel.Font = UIFont.FromName("OpenSans", 10);
            RaggiungiciButtonLabel.Lines = 1;
            RaggiungiciButtonLabel.Text = RaggiungiciButtontxt;
            RaggiungiciButton.Add(RaggiungiciButtonLabel);

            RaggiungiciButton.Frame = new CGRect(55, Y + 5, 35 + RaggiungiciButtonSize.Width, 30);
                
            Y += (int)RaggiungiciButton.Frame.Height + 5;

            string Telefonotxt = NSBundle.MainBundle.LocalizedString("Contatti_Telefono", "", null);
            UIImage TelefonoImage = UIImage.FromFile("Contatti_Telefono.png");
            var TelefonoView = CreateContactDataView(TelefonoImage, Telefonotxt, DataFont, new CGPoint(15, Y + 15), (float)MainView.Frame.Width - 30);
            scrollView.Add(TelefonoView);

            Y += (int)TelefonoView.Frame.Height + 15;

            string EMailtxt = NSBundle.MainBundle.LocalizedString("Contatti_Mail", "", null);
            UIImage EMailImage = UIImage.FromFile("Contatti_Mail.png");
            var EMailView = CreateContactDataView(EMailImage, EMailtxt, DataFont, new CGPoint(15, Y + 15), (float)MainView.Frame.Width - 30);
            scrollView.Add(EMailView);

            Y += (int)EMailView.Frame.Height + 15;


            string ContactUStxt = NSBundle.MainBundle.LocalizedString("Contatti_TxtInfo", "", null);
            var ContactUSSize = UIStringDrawing.StringSize(ContactUStxt, UIFont.FromName("OpenSans", 10), new CGSize(View.Frame.Width - 30, 2000));

            UILabel ContactUSLabel = new UILabel(new CGRect(15, Y+15, View.Frame.Width - 30, ContactUSSize.Height));
            ContactUSLabel.TextColor = UIColor.Black;
            ContactUSLabel.TextAlignment = UITextAlignment.Center;
            ContactUSLabel.Font = UIFont.FromName("OpenSans", 10);
            ContactUSLabel.Lines = 0;
            ContactUSLabel.Text = ContactUStxt;
            scrollView.Add(ContactUSLabel);

            Y += (int)ContactUSLabel.Frame.Height + 15;

            string Nometxt = NSBundle.MainBundle.LocalizedString("Contatti_Nome", "", null);
            string Mailtxt = NSBundle.MainBundle.LocalizedString("Contatti_Email", "", null);
            string Messaggiotxt = NSBundle.MainBundle.LocalizedString("Contatti_Messaggio", "", null);
            var ContactUSLabelSize = UIStringDrawing.StringSize(Messaggiotxt, UIFont.FromName("OpenSans", 14), new CGSize(View.Frame.Width - 30, 2000));

            UILabel NomeLabel = new UILabel(new CGRect(15, (Y + 20) + 15 - ContactUSLabelSize.Height / 2, ContactUSLabelSize.Width, ContactUSLabelSize.Height));
            NomeLabel.TextColor = UIColor.Black;
            NomeLabel.Font = UIFont.FromName("OpenSans", 14);
            NomeLabel.Lines = 1;
            NomeLabel.Text = Nometxt;
            scrollView.Add(NomeLabel);

            UIView NomeView = new UIView(new CGRect(15 + ContactUSLabelSize.Width + 10 , (Y + 20), View.Frame.Width - (15 + ContactUSLabelSize.Width + 10 + 15), 30));
            NomeView.BackgroundColor = Rosso;
            NomeView.ClipsToBounds = true;
            NomeView.Layer.CornerRadius = 5f;
            scrollView.Add(NomeView);

            UIView NomeTxtBack = new UIView(new CGRect(1, 1, NomeView.Frame.Width - 2, NomeView.Frame.Height - 2));
            NomeTxtBack.BackgroundColor = UIColor.FromRGB(237, 237, 237);
            NomeTxtBack.Layer.CornerRadius = 5f;
            NomeView.Add(NomeTxtBack);

            UITextField NomeTxtField = new UITextField(new CGRect(5, 5, NomeTxtBack.Frame.Width - 10, 20));
            NomeTxtField.ReturnKeyType = UIReturnKeyType.Done;
            NomeTxtField.TextColor = UIColor.Black;
            NomeTxtField.Font = UIFont.FromName("OpenSans", 14);
            NomeTxtField.KeyboardType = UIKeyboardType.Default;
            NomeTxtField.ShouldReturn += (UITextField) => {
                flagNome = false;
                UITextField.ResignFirstResponder();
                return true;
            };
            NomeTxtBack.Add(NomeTxtField);

            Y += 20 + (int)NomeView.Frame.Height;

            UILabel MailLabel = new UILabel(new CGRect(15, (Y + 15) + 15 - ContactUSLabelSize.Height / 2, ContactUSLabelSize.Width, ContactUSLabelSize.Height));
            MailLabel.TextColor = UIColor.Black;
            MailLabel.Font = UIFont.FromName("OpenSans", 14);
            MailLabel.Lines = 1;
            MailLabel.Text = Mailtxt;
            scrollView.Add(MailLabel);

            UIView MailView = new UIView(new CGRect(15 + ContactUSLabelSize.Width + 10, (Y + 15), View.Frame.Width - (15 + ContactUSLabelSize.Width + 10 + 15), 30));
            MailView.BackgroundColor = Rosso;
            MailView.ClipsToBounds = true;
            MailView.Layer.CornerRadius = 5f;
            scrollView.Add(MailView);

            UIView MailTxtBack = new UIView(new CGRect(1, 1, MailView.Frame.Width - 2, MailView.Frame.Height - 2));
            MailTxtBack.BackgroundColor = UIColor.FromRGB(237, 237, 237);
            MailTxtBack.Layer.CornerRadius = 5f;
            MailView.Add(MailTxtBack);

            UITextField MailTxtField = new UITextField(new CGRect(5, 5, MailTxtBack.Frame.Width - 10, 20));
            MailTxtField.ReturnKeyType = UIReturnKeyType.Done;
            MailTxtField.TextColor = UIColor.Black;
            MailTxtField.Font = UIFont.FromName("OpenSans", 14);
            MailTxtField.KeyboardType = UIKeyboardType.EmailAddress;
            MailTxtField.ShouldReturn += (UITextField) => {
                flagEmail = false;
                UITextField.ResignFirstResponder();
                return true;
            };
            MailTxtBack.Add(MailTxtField);

            Y += 15 + (int)MailView.Frame.Height;

            UILabel MessaggioLabel = new UILabel(new CGRect(15, (Y + 15) + 15 - ContactUSLabelSize.Height / 2, ContactUSLabelSize.Width, ContactUSLabelSize.Height));
            MessaggioLabel.TextColor = UIColor.Black;
            MessaggioLabel.Font = UIFont.FromName("OpenSans", 14);
            MessaggioLabel.Lines = 1;
            MessaggioLabel.Text = Messaggiotxt;
            scrollView.Add(MessaggioLabel);

            UIView MessaggioView = new UIView(new CGRect(15 + ContactUSLabelSize.Width + 10, (Y + 15), View.Frame.Width - (15 + ContactUSLabelSize.Width + 10 + 15), 100));
            MessaggioView.BackgroundColor = Rosso;
            MessaggioView.ClipsToBounds = true;
            MessaggioView.Layer.CornerRadius = 5f;
            scrollView.Add(MessaggioView);

            UIView MessaggioTxtBack = new UIView(new CGRect(1, 1, MessaggioView.Frame.Width - 2, MessaggioView.Frame.Height - 2));
            MessaggioTxtBack.BackgroundColor = UIColor.FromRGB(237, 237, 237);
            MessaggioTxtBack.Layer.CornerRadius = 5f;
            MessaggioView.Add(MessaggioTxtBack);

            UITextView MessaggioTxtField = new UITextView(new CGRect(5, 5, MessaggioTxtBack.Frame.Width - 10, 88));
            //MessaggioTxtField.ReturnKeyType = UIReturnKeyType.Done;
            MessaggioTxtField.BackgroundColor = UIColor.FromRGB(237, 237, 237);
            MessaggioTxtField.TextColor = UIColor.Black;
            MessaggioTxtField.Font = UIFont.FromName("OpenSans", 14);
            MessaggioTxtField.KeyboardType = UIKeyboardType.Default;
            MessaggioTxtBack.Add(MessaggioTxtField);

            Y += 15 + (int)MessaggioView.Frame.Height;


            UIView SendButton = new UIView(new CGRect(View.Frame.Width/2 - 70, Y + 15, 140, 50));
            SendButton.BackgroundColor = Rosso;
            SendButton.Layer.CornerRadius = 5;
            UITapGestureRecognizer SendButtonTap = new UITapGestureRecognizer(() => {
                View.EndEditing(true);

                APIContatti(NomeTxtField.Text, MailTxtField.Text, MessaggioTxtField.Text);

            });
            SendButton.AddGestureRecognizer(SendButtonTap);
            SendButton.UserInteractionEnabled = true;
            scrollView.Add(SendButton);

            string SendButtontxt = NSBundle.MainBundle.LocalizedString("Contatti_Invia", "", null);
            var SendButtonSize = UIStringDrawing.StringSize(SendButtontxt, UIFont.FromName("OpenSans-Bold", 14), new CGSize(120, 30));

            UILabel SendButtonLabel = new UILabel(new CGRect(SendButton.Frame.Width / 2 - SendButtonSize.Width / 2, SendButton.Frame.Height/2 - SendButtonSize.Height / 2, SendButtonSize.Width, SendButtonSize.Height));
            SendButtonLabel.TextColor = UIColor.White;
            SendButtonLabel.Font = UIFont.FromName("OpenSans-Bold", 14);
            SendButtonLabel.Lines = 1;
            SendButtonLabel.Text = SendButtontxt;
            SendButton.Add(SendButtonLabel);

            Y += (int)SendButton.Frame.Height + 15;

            UIView FBButton = new UIView(new CGRect(View.Frame.Width / 2 - 80, Y + 30, 160, 60));
            FBButton.BackgroundColor = Rosso;
            FBButton.Layer.CornerRadius = 5;
            UITapGestureRecognizer FBButtonTap = new UITapGestureRecognizer(() => {
                View.EndEditing(true);

                string url = "https://www.facebook.com/vulandraviaggi/";
                url = url.Replace(" ", "%20");
                if (UIApplication.SharedApplication.CanOpenUrl(new NSUrl(url)))
                {
                    UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
                }
                else
                {
                    new UIAlertView("Error", "Facebook is not supported on this device", null, "Ok").Show();
                }

            });
            FBButton.AddGestureRecognizer(FBButtonTap);
            FBButton.UserInteractionEnabled = true;
            scrollView.Add(FBButton);

            UIView FBButtonBack = new UIView(new CGRect(1, 1, FBButton.Frame.Width-2, FBButton.Frame.Height - 2));
            FBButtonBack.BackgroundColor = UIColor.White;
            FBButtonBack.Layer.CornerRadius = 5;
            FBButton.Add(FBButtonBack);

            UIImageView FBButtonImage = new UIImageView(new CGRect(10, FBButton.Frame.Height/2 - 20, 40, 40));
            FBButtonImage.Image = UIImage.FromFile("Contatti_FB.png");
            FBButton.Add(FBButtonImage);


            string FBButtontxt = NSBundle.MainBundle.LocalizedString("Contatti_SeguiciSuFB", "", null);
            var FBButtonSize = UIStringDrawing.StringSize(FBButtontxt, UIFont.FromName("OpenSans", 14), new CGSize(90, 50));

            UILabel FBButtonLabel = new UILabel(new CGRect(60, FBButton.Frame.Height / 2 - FBButtonSize.Height / 2, FBButtonSize.Width, FBButtonSize.Height));
            FBButtonLabel.TextColor = Rosso;
            FBButtonLabel.Font = UIFont.FromName("OpenSans", 14);
            FBButtonLabel.Lines = 0;
            FBButtonLabel.Text = FBButtontxt;
            FBButton.Add(FBButtonLabel);

            Y += (int)FBButton.Frame.Height + 30;

            scrollView.ContentSize = new CGSize(View.Frame.Width, Y + 15);

            View.Add(NavigationBar);
            View.Add(TitleView);
            View.Add(MainView);

            DismissKeyboardOnBackgroundTap();

            // INIT MENU

            menu = new UIMenuView(new CGRect(0, 70, View.Frame.Width, View.Frame.Height - 70));
            menu.SetBackgroundColor(UIColor.FromRGBA(0, 0, 0, 75));
            menu.SetMenuColor(UIColor.FromRGB(102, 104, 162));
            menu.PAGE_TAG = "5";
            menu.CellHeight = 50;
            menu.CellWidht = 200;
            menu.MenuImg = MenuImage;

            menu.AddElement(GbsInstance.ListMenuElement);

            View.Add(menu);



            NomeTxtField.ShouldBeginEditing = delegate {
                flagNome = true;
                CGRect frame = View.Frame;
                //if (View.Frame.Height == 480)
                //{
                    frame.Y = -85;
                    View.Frame = frame;
                //}

                return true;
            };

            NomeTxtField.ShouldEndEditing = delegate {

                CGRect frame = View.Frame;
                frame.Y = 0;
                if (!flagNome)
                    View.Frame = frame;
                return true;
            };

            MailTxtField.ShouldBeginEditing = delegate {
                flagEmail = true;
                CGRect frame = View.Frame;
                //if (View.Frame.Height == 480)
                //{
                frame.Y = -135;
                View.Frame = frame;
                //}

                return true;
            };

            MailTxtField.ShouldEndEditing = delegate {

                CGRect frame = View.Frame;
                frame.Y = 0;
                if (!flagEmail)
                    View.Frame = frame;
                return true;
            };

            MessaggioTxtField.ShouldBeginEditing = delegate {
                flagMessaggio = true;
                CGRect frame = View.Frame;
                //if (View.Frame.Height == 480)
                //{
                frame.Y = -205;
                View.Frame = frame;
                //}

                return true;
            };

            MessaggioTxtField.ShouldEndEditing = delegate {

                CGRect frame = View.Frame;
                frame.Y = 0;
                if (!flagMessaggio)
                    View.Frame = frame;
                return true;
            };
        }



        public UIView CreateContactDataView(UIImage image, string text, UIFont font, CGPoint point, float widht)
        {
            UIView DataView = new UIView();

            float IMGW = 25;
            float IMGH = 25;

            UIImageView imageView= new UIImageView();
            imageView.Image = image;
            DataView.Add(imageView);

            var TextSize = UIStringDrawing.StringSize(text, font, new CGSize(widht - (IMGW + 15), 2000));

            UILabel Label = new UILabel();
            Label.TextColor = UIColor. Black;
            Label.Font = font;
            Label.Lines = 0;
            Label.Text = text;
            DataView.Add(Label);

            float VH;
            if (TextSize.Height > IMGH)
            {
                VH = (float)TextSize.Height;
            }
            else
            {
                VH = IMGH;
            }

            imageView.Frame = new CGRect(0, VH / 2 - IMGH / 2, IMGW, IMGH);
            Label.Frame = new CGRect(IMGW+15, VH / 2 - TextSize.Height / 2, TextSize.Width, TextSize.Height);

            DataView.Frame = new CGRect(point.X, point.Y, widht, VH);
                
            return DataView;
        }

        public void DismissKeyboardOnBackgroundTap()
        {
            // Add gesture recognizer to hide keyboard
            var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
            tap.AddTarget(() =>
            {
                flagNome = false;
                flagEmail = false;
                flagMessaggio = false;

                View.EndEditing(true);
            });
            View.AddGestureRecognizer(tap);
        }

        public void APIContatti(string nome, string email, string messaggio){
                              
            bool flagNome = false;
            bool flagEMail = false;
            bool flagMessaggio = false;


            if (string.IsNullOrEmpty(nome))
            {
                flagNome = true;
            }
            if (string.IsNullOrEmpty(messaggio))
            {
                flagMessaggio = true;
            }

            if (string.IsNullOrEmpty(email))
            {
                flagEMail = true;
            }
            else
            {
                try
                {
                    MailAddress m = new MailAddress(email);
                }
                catch (FormatException)
                {
                    flagEMail = true;
                }
            }


            if (flagEMail || flagNome  || flagMessaggio)
            {
                Toast.MakeToast("Errore dati non inseriti o errati").Show();
                return;

            }

            var client = new RestClient(GbsInstance.BaseUrl);

            var requestN4U = new RestRequest("mail_app.php", Method.POST);
            requestN4U.AddHeader("content-type", "application/json");
            //requestN4U.AddParameter("application/x-www-form-urlencoded", $"post={"3"}&email={"matteo.lomi@netwintec.com"}&nome={"Matteo"}&messaggio={"Test Messaggio"}", ParameterType.RequestBody);
            JObject oJsonObject = new JObject();

            oJsonObject.Add("tipo", "3");
            oJsonObject.Add("email", email);
            oJsonObject.Add("nome", nome);
            oJsonObject.Add("messaggio", messaggio);

            requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);
            Console.WriteLine(client.BaseUrl + "" + requestN4U.Resource);

            client.ExecuteAsync(requestN4U,(response, arg2) => {
                
                Console.WriteLine("StatusCode mail_app.php:" + response.StatusCode +
                                  "\nContent mail_app.php:" + response.Content);

                if(response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    InvokeOnMainThread(() =>
                    {
                        Toast.MakeToast("Email inviata con successo").Show();
                    });
                }
                else
                {
                    InvokeOnMainThread(() =>
                    {
                        Toast.MakeToast("Errore riprovare più tardi").Show();
                    });
                }

            });

           
        } 


    }
}