﻿using System;

namespace Vulandra
{
	public class DestinazioniObj 
	{
		public string status { get; set; }
		public titleDetails title { get; set; }
	}

	public class titleDetails
	{
		public string rendered { get; set; }
	}
}
