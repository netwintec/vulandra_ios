﻿using System;
namespace Vulandra
{
    public class OfferteObj
    {
        public int id { get; set; }
        public TitleObj title { get; set; }
        public ACFObj acf { get; set; }
        public ImagesObj images { get; set; }
    }

    public class TitleObj
    {
        public string rendered { get; set; }
    }

    public class ACFObj
    {
        public string categoria { get; set; }
        public string link { get; set; }
        public string descrizione { get; set; }
    }
    public class ImagesObj
    {
        public string medium { get; set; }
        public string large { get; set; }
    }
}
