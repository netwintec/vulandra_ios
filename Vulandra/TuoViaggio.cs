﻿using CoreGraphics;
using Foundation;
using GlobalToast;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using UIKit;

namespace Vulandra
{
    public partial class TuoViaggio : UIViewController
    {
        GlobalStructureInstance GbsInstance = GlobalStructureInstance.Instance;

        UIColor Viola = UIColor.FromRGB(134, 63, 145);
        UIMenuView menu;

        UIScrollView scrollView;

        UIView ModalBackground;
        DateTime DaDate, ADate;
        bool DaOpen = false, AOpen = false;

        bool NoSend = false;

        public static TuoViaggio Instance { private set; get; }

        public TuoViaggio(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidAppear(bool animated)
        {
            try
            {
                GbsInstance.SetPage(PageName.IlTuoViaggio);
            }
            catch (Exception) { };

            base.ViewDidAppear(animated);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            GbsInstance.SetPage(PageName.IlTuoViaggio);

            TuoViaggio.Instance = this;

            UIView NavigationBar = new UIView(new CGRect(0, 0, View.Frame.Width, 70));
            NavigationBar.BackgroundColor = UIColor.FromRGB(102, 104, 162);

            UIImageView MenuImage = new UIImageView(new CGRect(15, 27.5, 35, 35));
            MenuImage.Image = UIImage.FromFile("MenuClose.png");
            var imTap = new UITapGestureRecognizer(() => {
                if (menu.MenuOpen)
                {
                    menu.CloseMenu();
                }
                else
                {
                    menu.OpenMenu();
                }
            });
            MenuImage.UserInteractionEnabled = true;
            MenuImage.AddGestureRecognizer(imTap);
            NavigationBar.Add(MenuImage);

            UIImageView BackImage = new UIImageView(new CGRect(View.Frame.Width - 50, 27.5, 35, 35));
            BackImage.Image = UIImage.FromFile("Back.png");
            var backTap = new UITapGestureRecognizer(() => {
                this.NavigationController.PopToRootViewController(true);
            });
            BackImage.UserInteractionEnabled = true;
            BackImage.AddGestureRecognizer(backTap);
            NavigationBar.Add(BackImage);

            UIImageView LogoImage = new UIImageView(new CGRect(View.Frame.Width / 2 - 52.5, 27.5, 105, 35));
            LogoImage.Image = UIImage.FromFile("Logo.png");
            NavigationBar.Add(LogoImage);

            UIView Sep = new UIView(new CGRect(0, 68, View.Frame.Width, 2));
            Sep.BackgroundColor = Viola;
            NavigationBar.Add(Sep);

            UIView TitleView = new UIView(new CGRect(0, 70, View.Frame.Width, 110));
			TitleView.BackgroundColor = Viola;

            string Titletxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_Tit", "", null);
            var TitleSize = UIStringDrawing.StringSize(Titletxt, UIFont.FromName("OpenSans-Bold", 18), new CGSize(View.Frame.Width - 30, 80));

            UILabel TitleLabel = new UILabel(new CGRect(15, 60 / 2 - TitleSize.Height / 2, View.Frame.Width - 30, TitleSize.Height));
            TitleLabel.TextColor = UIColor.White;
            TitleLabel.TextAlignment = UITextAlignment.Center;
            TitleLabel.Font = UIFont.FromName("OpenSans-Bold", 18);
            TitleLabel.Lines = 0;
            TitleLabel.Text = Titletxt;
            TitleView.Add(TitleLabel);

            UIView SepTitle = new UIView(new CGRect(0, 60, View.Frame.Width, 50));
            SepTitle.BackgroundColor = Viola;
            TitleView.Add(SepTitle);

            string Descrtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_Descr", "", null);
            var DescrSize = UIStringDrawing.StringSize(Descrtxt, UIFont.FromName("OpenSans", 14), new CGSize(View.Frame.Width - 30, 50));

            UILabel DescrLabel = new UILabel(new CGRect(15, 25 - DescrSize.Height/2, View.Frame.Width - 30, DescrSize.Height));
            DescrLabel.TextColor = UIColor.White;
            DescrLabel.TextAlignment = UITextAlignment.Center;
            DescrLabel.Font = UIFont.FromName("OpenSans", 14);
            DescrLabel.Lines = 0;
            DescrLabel.Text = Descrtxt;
            SepTitle.Add(DescrLabel);

			UIView MainView = new UIView(new CGRect(0, 180, View.Frame.Width, View.Frame.Height - 180));
            MainView.BackgroundColor = UIColor.White;

            scrollView = new UIScrollView(new CGRect(0, 0, MainView.Frame.Width, MainView.Frame.Height));
            scrollView.Bounces = false;
            MainView.Add(scrollView);

            int Y = 0;
            UIFont DataFont = UIFont.FromName("OpenSans", 14);

            string DatiPtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_DatiTit", "", null);
            var DatiPSize = UIStringDrawing.StringSize(DatiPtxt, UIFont.FromName("OpenSans-Bold", 16), new CGSize(View.Frame.Width - 30, 50));

            UILabel DatiPTit = new UILabel(new CGRect(15, Y + 20, DatiPSize.Width, DatiPSize.Height));
            DatiPTit.TextColor = UIColor.Black;
            DatiPTit.Font = UIFont.FromName("OpenSans-Bold", 16);
            DatiPTit.Lines = 0;
            DatiPTit.Text = DatiPtxt;
            scrollView.Add(DatiPTit);

            Y += 20 +(int)DatiPSize.Height;

            string Nometxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_NomeTit", "", null);
            string NomePhtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_NomeTit", "", null);
            var NomeView = CreateSimpleInputView(Nometxt, NomePhtxt, DataFont, new CGRect(0, Y + 15, View.Frame.Width, 30)); 
            scrollView.Add(NomeView);

            Y += 15 + (int)NomeView.Frame.Height;

            string Cognometxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_CognomeTit", "", null);
            string CognomePhtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_CognomeTit", "", null);
            var CognomeView = CreateSimpleInputView(Cognometxt, CognomePhtxt, DataFont, new CGRect(0, Y + 15, View.Frame.Width, 30));
            scrollView.Add(CognomeView);

            Y += 15 + (int)CognomeView.Frame.Height;

            string Mailtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_EMailTit", "", null);
            string MailPhtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_EMailTit", "", null);
            var MailView = CreateSimpleInputView(Mailtxt, MailPhtxt, DataFont, new CGRect(0, Y + 15, View.Frame.Width, 30));

            var Mailfield = MailView.ViewWithTag(1) as UITextField;
            Mailfield.KeyboardType = UIKeyboardType.EmailAddress;

            scrollView.Add(MailView);

            Y += 15 + (int)MailView.Frame.Height;

            string Telefonotxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_TelTit", "", null);
            string TelefonoPhtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_TelTit", "", null);
            var TelefonoView = CreateSimpleInputView(Telefonotxt, TelefonoPhtxt, DataFont, new CGRect(0, Y + 15, View.Frame.Width, 30));

            var telefonofield = TelefonoView.ViewWithTag(1) as UITextField;
            telefonofield.KeyboardType = UIKeyboardType.NumberPad;
            scrollView.Add(TelefonoView);

            Y += 15 + (int)TelefonoView.Frame.Height;

            UISwitch LISswitch = new UISwitch(new CGRect(15, Y + 15, 40, 30));
            LISswitch.On = false;
            LISswitch.OnTintColor = Viola;
            scrollView.Add(LISswitch);

            string LIStxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_ContattoLIS", "", null);
            var LISSize = UIStringDrawing.StringSize(LIStxt, UIFont.FromName("OpenSans", 14), new CGSize(View.Frame.Width - 85, 30));
            UILabel LISTit = new UILabel(new CGRect(70, Y + 15 + 15 - LISSize.Height / 2, LISSize.Width, LISSize.Height));
            LISTit.TextColor = UIColor.Black;
            LISTit.Font = UIFont.FromName("OpenSans", 14);
            LISTit.Lines = 0;
            LISTit.Text = LIStxt;
            scrollView.Add(LISTit);

            Y += 45;

            UISwitch WHswitch = new UISwitch(new CGRect(15, Y + 15, 40, 30));
            WHswitch.On = false;
            WHswitch.OnTintColor = Viola;
            scrollView.Add(WHswitch);

            string WHtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_ContattoWH", "", null);
            var WHSize = UIStringDrawing.StringSize(WHtxt, UIFont.FromName("OpenSans", 14), new CGSize(View.Frame.Width - 85, 30));
            UILabel WHTit = new UILabel(new CGRect(70, Y + 15 + 15 - WHSize.Height / 2, WHSize.Width, WHSize.Height));
            WHTit.TextColor = UIColor.Black;
            WHTit.Font = UIFont.FromName("OpenSans", 14);
            WHTit.Lines = 0;
            WHTit.Text = WHtxt;
            scrollView.Add(WHTit);

            Y += 45;

            string Destinazionitxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_DestTit", "", null);
            var DestinazioniSize = UIStringDrawing.StringSize(Destinazionitxt, UIFont.FromName("OpenSans-Bold", 16), new CGSize(View.Frame.Width - 30, 50));

            UILabel DestinazioniTit = new UILabel(new CGRect(15, Y + 20, DestinazioniSize.Width, DestinazioniSize.Height));
            DestinazioniTit.TextColor = UIColor.Black;
            DestinazioniTit.Font = UIFont.FromName("OpenSans-Bold", 16);
            DestinazioniTit.Lines = 0;
            DestinazioniTit.Text = Destinazionitxt;
            scrollView.Add(DestinazioniTit);

            Y += 20 + (int)DestinazioniTit.Frame.Height;

            string valueSel = "";
            UIPickerView pickerStatus = new UIPickerView(new CGRect(15, Y+5, 200, 90));
            if (GlobalStructureInstance.Instance.ListDestinazioni == null)
            {
                GlobalStructureInstance.Instance.ListDestinazioni = new List<string>();
            }
            else
            {
                if(GlobalStructureInstance.Instance.ListDestinazioni.Count != 0)
                    valueSel = GlobalStructureInstance.Instance.ListDestinazioni[0];
            }
            var pickerData = new StatusPickerViewModel(GlobalStructureInstance.Instance.ListDestinazioni);
            pickerStatus.Model = pickerData;
            pickerData.ValueChanged += delegate
            {
                valueSel = pickerData.SelectedItem;
            };
            scrollView.Add(pickerStatus);

            Y += 15 + (int)pickerStatus.Frame.Height;

            string Quandotxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_WhenTit", "", null);
            var QuandoSize = UIStringDrawing.StringSize(Quandotxt, UIFont.FromName("OpenSans-Bold", 16), new CGSize(View.Frame.Width - 30, 50));

            UILabel QuandoTit = new UILabel(new CGRect(15, Y + 20, QuandoSize.Width, QuandoSize.Height));
            QuandoTit.TextColor = UIColor.Black;
            QuandoTit.Font = UIFont.FromName("OpenSans-Bold", 16);
            QuandoTit.Lines = 0;
            QuandoTit.Text = Quandotxt;
            scrollView.Add(QuandoTit);

            Y += 20 + (int)QuandoTit.Frame.Height;

            string Daltxt = NSBundle.MainBundle.LocalizedString("ViaggioIdeale_Da", "", null);
            var DalSize = UIStringDrawing.StringSize(Daltxt, UIFont.FromName("OpenSans", 14), new CGSize(2000, 30));

            string Altxt = NSBundle.MainBundle.LocalizedString("ViaggioIdeale_A", "", null);
            var AlSize = UIStringDrawing.StringSize(Daltxt, UIFont.FromName("OpenSans", 14), new CGSize(2000, 30));

            UILabel DalLabel = new UILabel(new CGRect(15, Y + 15 + 15 - DalSize.Height / 2, DalSize.Width, DalSize.Height));
            DalLabel.TextColor = UIColor.Black;
            DalLabel.Font = UIFont.FromName("OpenSans", 14);
            DalLabel.Lines = 1;
            DalLabel.Text = Daltxt;
            scrollView.Add(DalLabel);

            var DataViewW = ((View.Frame.Width - 30) - AlSize.Width - DalSize.Width - 10 - 10) / 2;

            UIView DalDataView = new UIView(new CGRect(DalLabel.Frame.X + DalLabel.Frame.Width + 5, Y+15, DataViewW, 30));
            DalDataView.BackgroundColor = Viola;
            DalDataView.ClipsToBounds = true;
            DalDataView.Layer.CornerRadius = 5f;
            scrollView.Add(DalDataView);


            UIView DalDataTxtBack = new UIView(new CGRect(1, 1, DalDataView.Frame.Width - 2, DalDataView.Frame.Height - 2));
            DalDataTxtBack.BackgroundColor = UIColor.FromRGB(237, 237, 237);
            DalDataTxtBack.Layer.CornerRadius = 5f;
            DalDataView.Add(DalDataTxtBack);

            UILabel DaDataTxt = new UILabel(new CGRect(5, 5, DataViewW - 5 - 30 - 5, 20));
            DaDataTxt.TextColor = UIColor.Black;
            DaDataTxt.Font = UIFont.FromName("OpenSans-Bold", 13);
            DaDataTxt.Lines = 1;
            DalDataTxtBack.Add(DaDataTxt);

            UIView DalDataImageBack = new UIView(new CGRect(DalDataView.Frame.Width - DalDataView.Frame.Height, 0, DalDataView.Frame.Height, DalDataView.Frame.Height));
            DalDataImageBack.BackgroundColor = Viola;
            DalDataView.Add(DalDataImageBack);

            UIImageView DalDataImage = new UIImageView(new CGRect(4, 4, 20, 20));
            DalDataImage.Image = UIImage.FromFile("CalendarIcon.png");
            DalDataImageBack.Add(DalDataImage);

            UILabel AlLabel = new UILabel(new CGRect(DalDataView.Frame.X + DalDataView.Frame.Width + 10, Y + 15 + 15 - DalSize.Height / 2, DalSize.Width, DalSize.Height));
            AlLabel.TextColor = UIColor.Black;
            AlLabel.Font = UIFont.FromName("OpenSans", 14);
            AlLabel.Lines = 1;
            AlLabel.Text = Altxt;
            scrollView.Add(AlLabel);

            UIView AlDataView = new UIView(new CGRect(AlLabel.Frame.X + AlLabel.Frame.Width + 5, Y + 15, DataViewW, 30));
            AlDataView.BackgroundColor = Viola;
            AlDataView.ClipsToBounds = true;
            AlDataView.Layer.CornerRadius = 5f;
            scrollView.Add(AlDataView);

            UIView AlDataTxtBack = new UIView(new CGRect(1, 1, AlDataView.Frame.Width - 2, AlDataView.Frame.Height - 2));
            AlDataTxtBack.BackgroundColor = UIColor.FromRGB(237, 237, 237);
            AlDataTxtBack.Layer.CornerRadius = 5f;
            AlDataView.Add(AlDataTxtBack);

            UILabel ADataTxt = new UILabel(new CGRect(5, 5, DataViewW - 5 - 30 - 5, 20));
            ADataTxt.TextColor = UIColor.Black;
            ADataTxt.Font = UIFont.FromName("OpenSans-Bold", 13);
            ADataTxt.Lines = 1;
            AlDataTxtBack.Add(ADataTxt);

            UIView AlDataImageBack = new UIView(new CGRect(AlDataView.Frame.Width - AlDataView.Frame.Height, 0, AlDataView.Frame.Height, AlDataView.Frame.Height));
            AlDataImageBack.BackgroundColor = Viola;
            AlDataView.Add(AlDataImageBack);

            UIImageView AlDataImage = new UIImageView(new CGRect(4, 4, 20, 20));
            AlDataImage.Image = UIImage.FromFile("CalendarIcon.png");
            AlDataImageBack.Add(AlDataImage);


            DateTime now = DateTime.Now;

            DaDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            ADate = DaDate.AddDays(1);

            DaDataTxt.Text = FormatDateTime(DaDate);
            ADataTxt.Text = FormatDateTime(ADate);

            Y += 15 + 30;


            string Autotxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_AutoTit", "", null);
            var AutoSize = UIStringDrawing.StringSize(Autotxt, UIFont.FromName("OpenSans-Bold", 16), new CGSize(View.Frame.Width - 30, 50));

            UILabel AutoTit = new UILabel(new CGRect(15, Y + 20, AutoSize.Width, AutoSize.Height));
            AutoTit.TextColor = UIColor.Black;
            AutoTit.Font = UIFont.FromName("OpenSans-Bold", 16);
            AutoTit.Lines = 0;
            AutoTit.Text = Autotxt;
            scrollView.Add(AutoTit);

            Y += 20 + (int)AutoTit.Frame.Height;


            UISwitch Autoswitch = new UISwitch(new CGRect(15,Y+15,40,30));
            Autoswitch.On = false;
            Autoswitch.OnTintColor = Viola;
            scrollView.Add(Autoswitch);

            string AutoLtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_AutoDayTit", "", null);
            string AutoPhtxt = "giorni";
            var AutoView = CreateSimpleInputView(AutoLtxt, AutoPhtxt, DataFont, new CGRect(60, Y + 15, View.Frame.Width - 60, 30));
            AutoView.Alpha = 0;

            var Autofield = AutoView.ViewWithTag(1) as UITextField;
            Autofield.KeyboardType = UIKeyboardType.NumberPad;
            Autofield.TextAlignment = UITextAlignment.Center;
            scrollView.Add(AutoView);

            Autoswitch.ValueChanged += delegate {
                if(Autoswitch.On)
                {
                    AutoView.Alpha = 1;
                }
                else
                {
                    AutoView.Alpha = 0;
                }
            };

            Y += 15 + (int)AutoView.Frame.Height;


            string Viaggiatoritxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_ViaggiatoriTit", "", null);
            var ViaggiatoriSize = UIStringDrawing.StringSize(Viaggiatoritxt, UIFont.FromName("OpenSans-Bold", 16), new CGSize(View.Frame.Width - 30, 50));

            UILabel ViaggiatoriTit = new UILabel(new CGRect(15, Y + 20, ViaggiatoriSize.Width, ViaggiatoriSize.Height));
            ViaggiatoriTit.TextColor = UIColor.Black;
            ViaggiatoriTit.Font = UIFont.FromName("OpenSans-Bold", 16);
            ViaggiatoriTit.Lines = 0;
            ViaggiatoriTit.Text = Viaggiatoritxt;
            scrollView.Add(ViaggiatoriTit);

            Y += 20 + (int)ViaggiatoriTit.Frame.Height;

            string Adultitxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_AdultiTit", "", null);
            string AdultiPhtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_AdultiTit", "", null);
            var AdultiView = CreateSimpleInputView(Adultitxt, AdultiPhtxt, DataFont, new CGRect(0, Y + 15, View.Frame.Width/4 *3, 30));

            var Adultifield = AdultiView.ViewWithTag(1) as UITextField;
            Adultifield.KeyboardType = UIKeyboardType.NumberPad;
            Adultifield.TextAlignment = UITextAlignment.Center;
            scrollView.Add(AdultiView);

            Y += 15 + (int)AdultiView.Frame.Height;

            string Ragazzitxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_RagazziTit", "", null);
            string RagazziPhtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_RagazziTit", "", null);
            var RagazziView = CreateSimpleInputView(Ragazzitxt, RagazziPhtxt, DataFont, new CGRect(0, Y + 15, View.Frame.Width / 4 * 3, 30));

            var Ragazzifield = RagazziView.ViewWithTag(1) as UITextField;
            Ragazzifield.KeyboardType = UIKeyboardType.NumberPad;
            Ragazzifield.TextAlignment = UITextAlignment.Center;
            scrollView.Add(RagazziView);

            Y += 15 + (int)RagazziView.Frame.Height;

            string Bambinitxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_BambiniTit", "", null);
            string BambiniPhtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_BambiniTit", "", null);
            var BambiniView = CreateSimpleInputView(Bambinitxt, BambiniPhtxt, DataFont, new CGRect(0, Y + 15, View.Frame.Width / 4 * 3, 30));

            var Bambinifield = BambiniView.ViewWithTag(1) as UITextField;
            Bambinifield.KeyboardType = UIKeyboardType.NumberPad;
            Bambinifield.TextAlignment = UITextAlignment.Center;
            scrollView.Add(BambiniView);

            Y += 15 + (int)BambiniView.Frame.Height;

            string Animalitxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_AnimaliTit", "", null);
            string AnimaliPhtxt = "Animali";
            var AnimaliView = CreateSimpleInputView(Animalitxt, AnimaliPhtxt, DataFont, new CGRect(0, Y + 15, View.Frame.Width / 4 * 3, 30));

            var Animalifield = AnimaliView.ViewWithTag(1) as UITextField;
            Animalifield.KeyboardType = UIKeyboardType.NumberPad;
            Animalifield.TextAlignment = UITextAlignment.Center;
            scrollView.Add(AnimaliView);

            Y += 15 + (int)AnimaliView.Frame.Height;

            string Alloggiotxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_AlloggioTit", "", null);
            var AlloggioSize = UIStringDrawing.StringSize(Alloggiotxt, UIFont.FromName("OpenSans-Bold", 16), new CGSize(View.Frame.Width - 30, 50));

            UILabel AlloggioTit = new UILabel(new CGRect(15, Y + 20, AlloggioSize.Width, AlloggioSize.Height));
            AlloggioTit.TextColor = UIColor.Black;
            AlloggioTit.Font = UIFont.FromName("OpenSans-Bold", 16);
            AlloggioTit.Lines = 0;
            AlloggioTit.Text = Alloggiotxt;
            scrollView.Add(AlloggioTit);

            Y += 20 + (int)AlloggioTit.Frame.Height;

            UISwitch Casaswitch = new UISwitch(new CGRect(15, Y + 15, 40, 30));
            Casaswitch.On = false;
            Casaswitch.OnTintColor = Viola;
            scrollView.Add(Casaswitch);

            string Casatxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_Alloggio1", "", null);
            var CasaSize = UIStringDrawing.StringSize(Casatxt, UIFont.FromName("OpenSans", 14), new CGSize(View.Frame.Width - 85, 30));
            UILabel CasaTit = new UILabel(new CGRect(70, Y + 15 + 15 - CasaSize.Height/2, CasaSize.Width, CasaSize.Height));
            CasaTit.TextColor = UIColor.Black;
            CasaTit.Font = UIFont.FromName("OpenSans", 14);
            CasaTit.Lines = 0;
            CasaTit.Text = Casatxt;
            scrollView.Add(CasaTit);

            Y += 45;

            UISwitch Residenceswitch = new UISwitch(new CGRect(15, Y + 15, 40, 30));
            Residenceswitch.On = false;
            Residenceswitch.OnTintColor = Viola;
            scrollView.Add(Residenceswitch);

            string Residencetxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_Alloggio2", "", null);
            var ResidenceSize = UIStringDrawing.StringSize(Residencetxt, UIFont.FromName("OpenSans", 14), new CGSize(View.Frame.Width - 85, 30));
            UILabel ResidenceTit = new UILabel(new CGRect(70, Y + 15 + 15 - ResidenceSize.Height / 2, ResidenceSize.Width, ResidenceSize.Height));
            ResidenceTit.TextColor = UIColor.Black;
            ResidenceTit.Font = UIFont.FromName("OpenSans", 14);
            ResidenceTit.Lines = 0;
            ResidenceTit.Text = Residencetxt;
            scrollView.Add(ResidenceTit);

            Y += 45;

            UISwitch Hotelswitch = new UISwitch(new CGRect(15, Y + 15, 40, 30));
            Hotelswitch.On = false;
            Hotelswitch.OnTintColor = Viola;
            scrollView.Add(Hotelswitch);

            string Hoteltxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_Alloggio3", "", null);
            var HotelSize = UIStringDrawing.StringSize(Hoteltxt, UIFont.FromName("OpenSans", 14), new CGSize(View.Frame.Width - 85, 30));
            UILabel HotelTit = new UILabel(new CGRect(70, Y + 15 + 15 - HotelSize.Height / 2, HotelSize.Width, HotelSize.Height));
            HotelTit.TextColor = UIColor.Black;
            HotelTit.Font = UIFont.FromName("OpenSans", 14);
            HotelTit.Lines = 0;
            HotelTit.Text = Hoteltxt;
            scrollView.Add(HotelTit);

            Y += 45;

            UISwitch VTswitch = new UISwitch(new CGRect(15, Y + 15, 40, 30));
            VTswitch.On = false;
            VTswitch.OnTintColor = Viola;
            scrollView.Add(VTswitch);

            string VTtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_Alloggio4", "", null);
            var VTSize = UIStringDrawing.StringSize(VTtxt, UIFont.FromName("OpenSans", 14), new CGSize(View.Frame.Width - 85, 30));
            UILabel VTTit = new UILabel(new CGRect(70, Y + 15 + 15 - VTSize.Height / 2, VTSize.Width, VTSize.Height));
            VTTit.TextColor = UIColor.Black;
            VTTit.Font = UIFont.FromName("OpenSans", 14);
            VTTit.Lines = 0;
            VTTit.Text = VTtxt;
            scrollView.Add(VTTit);

            Y += 45;

            UIView Button = new UIView(new CGRect((View.Frame.Width - 30) / 2 - 100, Y + 30, 200, 60));
            Button.BackgroundColor = Viola;
            Button.Layer.CornerRadius = 5;
            UITapGestureRecognizer ButtonTap = new UITapGestureRecognizer(() =>
            {
                View.EndEditing(true);

                //nome,cognome,email,telefono,destinazione,data_partenza,data_fine,
                //autonoleggio,giorni_autonoleggio,adulti,bambini,ragazzi,animali,tipo_alloggio.

                if (!NoSend)
                {
                    var nome = (NomeView.ViewWithTag(1) as UITextField).Text;
                    var cognome = (CognomeView.ViewWithTag(1) as UITextField).Text;
                    var email = (MailView.ViewWithTag(1) as UITextField).Text;
                    var telefono = (TelefonoView.ViewWithTag(1) as UITextField).Text;
                    var destinazioni = valueSel;
                    var adulti = (AdultiView.ViewWithTag(1) as UITextField).Text;
                    var bambini = (BambiniView.ViewWithTag(1) as UITextField).Text;
                    var ragazzi = (RagazziView.ViewWithTag(1) as UITextField).Text;
                    var animali = (AnimaliView.ViewWithTag(1) as UITextField).Text;

                    string tipo_alloggio = "";
                    if (Casaswitch.On)
                        tipo_alloggio += "Casa,";
                    if (Residenceswitch.On)
                        tipo_alloggio += "Residence,";
                    if (Hotelswitch.On)
                        tipo_alloggio += "Hotel,";
                    if (VTswitch.On)
                        tipo_alloggio += "Villaggio Turistico,";

                    if (!string.IsNullOrEmpty(tipo_alloggio))
                        tipo_alloggio = tipo_alloggio.Substring(0, tipo_alloggio.Length - 1);
                    
                    APITuoViaggio(nome, cognome, email, telefono,
                                  LISswitch.On,WHswitch.On,
                                  destinazioni,
                                  DaDataTxt.Text, ADataTxt.Text,
                                  Autoswitch.On, Autofield.Text,
                                  adulti,bambini,ragazzi,animali,
                                  tipo_alloggio);
                }
            });
            Button.AddGestureRecognizer(ButtonTap);
            Button.UserInteractionEnabled = true;
            scrollView.Add(Button);


            UIImageView ButtonImage = new UIImageView(new CGRect(15, 15, 30, 30));
            ButtonImage.Image = UIImage.FromFile("ViaggioIdealeBtnIcon.png");
            Button.Add(ButtonImage);


            string Buttontxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_SendTxt", "", null);
            var ButtonSize = UIStringDrawing.StringSize(Buttontxt, UIFont.FromName("OpenSans", 14), new CGSize(130, 60));

            UILabel ButtonLabel = new UILabel(new CGRect(55, 30 - ButtonSize.Height / 2, ButtonSize.Width, ButtonSize.Height));
            ButtonLabel.TextColor = UIColor.White;
            ButtonLabel.TextAlignment = UITextAlignment.Center;
            ButtonLabel.Font = UIFont.FromName("OpenSans", 14);
            ButtonLabel.Lines = 0;
            ButtonLabel.Text = Buttontxt;
            Button.Add(ButtonLabel);

            var ButtonW = 15 + 30 + 10 + ButtonSize.Width + 15;
            Button.Frame = new CGRect((View.Frame.Width - 30) / 2 - ButtonW/2, Y + 30, ButtonW , 60);

            Y += 30 + 60;

            scrollView.ContentSize = new CGSize(View.Frame.Width, Y + 15);


            // MODAL

            ModalBackground = new UIView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height));
            ModalBackground.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 125);
            ModalBackground.Alpha = 0;

            UIView Modal = new UIView();
            Modal.BackgroundColor = UIColor.White;
            UITapGestureRecognizer ModalTap = new UITapGestureRecognizer(() => {
                //ModalBackground.Alpha = 0;
                View.EndEditing(true);
            });
            Modal.AddGestureRecognizer(ModalTap);
            Modal.UserInteractionEnabled = true;
            ModalBackground.Add(Modal);

            int ModalH = 180 + 30+20 +15+15;

            Modal.Frame = new CGRect(15, ModalBackground.Frame.Height / 2 - ModalH / 2, View.Frame.Width - 30, ModalH + 20);

            UIDatePicker PickerView = new UIDatePicker()
            {
                Frame = new CGRect(20, 15, Modal.Frame.Width - 40, 180),
            };
            PickerView.Mode = UIDatePickerMode.Date;
            PickerView.BackgroundColor = UIColor.White;
            PickerView.MinimumDate = (NSDate)DateTime.Now;
            Modal.Add(PickerView);

            UIButton PickerOK = new UIButton(new CGRect(Modal.Frame.Width / 2 - 30, PickerView.Frame.Y + 200, 60, 30));
            PickerOK.BackgroundColor = Viola;
            PickerOK.SetTitle("OK", UIControlState.Normal);
            PickerOK.SetTitleColor(UIColor.White, UIControlState.Normal);
            PickerOK.TouchUpInside += delegate {

                NSCalendar cal = NSCalendar.CurrentCalendar;
                var y = (int)cal.GetComponentFromDate(NSCalendarUnit.Year, PickerView.Date);
                var m = (int)cal.GetComponentFromDate(NSCalendarUnit.Month, PickerView.Date);
                var d = (int)cal.GetComponentFromDate(NSCalendarUnit.Day, PickerView.Date);

                if (DaOpen)
                {

                    DaDate = new DateTime(y, m, d, 0, 0, 0);
                    DaDataTxt.Text = FormatDateTime(DaDate);

                    if (ADate.CompareTo(DaDate) < 0)
                    {
                        ADate = DaDate.AddDays(1);
                        ADataTxt.Text = FormatDateTime(ADate);
                    }
                }

                if (AOpen)
                {
                    ADate = new DateTime(y, m, d, 0, 0, 0);
                    ADataTxt.Text = FormatDateTime(ADate);
                }


                DaOpen = false;
                AOpen = false;
                ModalBackground.Alpha = 0;

            };
            Modal.Add(PickerOK);

            UITapGestureRecognizer DalDataTap = new UITapGestureRecognizer(() => {
                View.EndEditing(true);

                DaOpen = true;
                AOpen = false;

                ModalBackground.Alpha = 1;

                PickerView.SetDate((NSDate)DateTime.SpecifyKind(DaDate, DateTimeKind.Local), false);
                //PickerView.MinimumDate = 0;
                PickerView.MinimumDate = (NSDate)DateTime.Now;

            });
            DalDataView.AddGestureRecognizer(DalDataTap);
            DalDataView.UserInteractionEnabled = true;

            UITapGestureRecognizer AlDataTap = new UITapGestureRecognizer(() => {
                View.EndEditing(true);

                DaOpen = false;
                AOpen = true;

                ModalBackground.Alpha = 1;

                PickerView.SetDate((NSDate)DateTime.SpecifyKind(ADate, DateTimeKind.Local), false);
                //PickerView.MinimumDate = 0;
                PickerView.MinimumDate = (NSDate)DateTime.SpecifyKind(DaDate.AddDays(1), DateTimeKind.Local);

            });
            AlDataView.AddGestureRecognizer(AlDataTap);
            AlDataView.UserInteractionEnabled = true;

            UITapGestureRecognizer ModalBackTap = new UITapGestureRecognizer(() => {
                ModalBackground.Alpha = 0;
            });
            ModalBackground.AddGestureRecognizer(ModalBackTap);
            ModalBackground.UserInteractionEnabled = true;

            // FINE MODAL





            View.Add(NavigationBar);
            View.Add(TitleView);
            View.Add(MainView);
            View.Add(ModalBackground);

            DismissKeyboardOnBackgroundTap();

            // INIT MENU

            menu = new UIMenuView(new CGRect(0, 70, View.Frame.Width, View.Frame.Height - 70));
            menu.SetBackgroundColor(UIColor.FromRGBA(0, 0, 0, 75));
            menu.SetMenuColor(UIColor.FromRGB(102, 104, 162));
            menu.PAGE_TAG = "5";
            menu.CellHeight = 50;
            menu.CellWidht = 200;
            menu.MenuImg = MenuImage;

            menu.AddElement(GbsInstance.ListMenuElement);

            View.Add(menu);

        }


        public UIView CreateSimpleInputView(string text,string placeholder, UIFont font, CGRect frame)
        {
            UIView DataView = new UIView();

            var TextSize = UIStringDrawing.StringSize(text, font, new CGSize(2000, 2000));

            var PHCAttr = new UIStringAttributes { Font = UIFont.FromName("OpenSans", 14), ForegroundColor = UIColor.FromRGB(180, 180, 180) };
            var phcNomeTxt = new NSMutableAttributedString(placeholder);
            phcNomeTxt.SetAttributes(PHCAttr.Dictionary, new NSRange(0, placeholder.Length));


            UILabel Label = new UILabel(new CGRect(15, frame.Height / 2 - TextSize.Height / 2, TextSize.Width, TextSize.Height));
            Label.TextColor = UIColor.Black;
            Label.Font = font;
            Label.Lines = 0;
            Label.Text = text;
            DataView.Add(Label);

            UITextField Field = new UITextField(new CGRect(15 + Label.Frame.Width + 10 , frame.Height / 2 - TextSize.Height / 2, frame.Width - (15 + Label.Frame.Width + 10 + 15), TextSize.Height));
            Field.TextColor = UIColor.Black;
            Field.Font = font;
            Field.Tag = 1;
            Field.Placeholder = placeholder;
            Field.KeyboardType = UIKeyboardType.Default;
            Field.ReturnKeyType = UIReturnKeyType.Done;
            Field.ShouldReturn += (UITextField) => { 
                UITextField.ResignFirstResponder();
                return true;
            };
            Field.ShouldBeginEditing = delegate {
                scrollView.ContentOffset = new CGPoint(0, DataView.Frame.Y - 10); 
                return true;
            };

            DataView.Add(Field);

            UIView Sep = new UIView(new CGRect(15 + Label.Frame.Width + 10, frame.Height -1, frame.Width - (15 + Label.Frame.Width + 10 + 15), 1));
            Sep.BackgroundColor = Viola;
            DataView.Add(Sep);

            DataView.Frame = frame;

            return DataView;
        }

        public void DismissKeyboardOnBackgroundTap()
        {
            // Add gesture recognizer to hide keyboard
            var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
            tap.AddTarget(() =>
            {
                //flagNome = false;
                //flagEmail = false;
                //flagMessaggio = false;

                View.EndEditing(true);
            });
            View.AddGestureRecognizer(tap);
        }

        public string FormatDateTime(DateTime dt)
        {

            string formatted = "";

            formatted = dt.ToString("dd MMM yyyy");

            return formatted;

        }

        //nome,cognome,email,telefono,destinazione,data_partenza,data_fine,autonoleggio,giorni_autonoleggio,adulti,bambini,ragazzi,animali,tipo_alloggio.
        public void APITuoViaggio(string nome, string cognome, string email, string telefono,
                                  bool lis, bool whatsapp,
                                  string destinazione, string data_partenza, string data_fine,
                                  bool autonoleggio, string giorni_autonoleggio, string adulti,
                                  string bambini, string ragazzi, string animali,
                                  string tipo_alloggio)
        {

            NoSend = true;

            bool flagNome = false;
            bool flagEMail = false;
            bool flagCognome = false;
            bool flagNoleggio = false;
            bool flagViaggiatori = false;
            bool flagAlloggio = false;


            if (string.IsNullOrEmpty(nome))
            {
                flagNome = true;
            }
            if (string.IsNullOrEmpty(cognome))
            {
                flagCognome = true;
            }
            if (autonoleggio && string.IsNullOrEmpty(nome))
            {
                flagNoleggio = true;
            }
            if (string.IsNullOrEmpty(adulti + bambini + ragazzi + animali))
            {
                flagViaggiatori = true;
            }
            if (string.IsNullOrEmpty(tipo_alloggio))
            {
                flagAlloggio = true;
            }

            if (string.IsNullOrEmpty(email))
            {
                flagEMail = true;
            }
            else
            {
                try
                {
                    MailAddress m = new MailAddress(email);
                }
                catch (FormatException)
                {
                    flagEMail = true;
                }
            }

            if (flagEMail || flagNome || flagCognome || flagNoleggio || flagViaggiatori || flagAlloggio)
            {
                NoSend = false;
                Toast.MakeToast("Errore dati non inseriti o errati").Show();
                return;

            }

            var client = new RestClient(GbsInstance.BaseUrl);

            var requestN4U = new RestRequest("mail_app.php", Method.POST);
            requestN4U.AddHeader("content-type", "application/json");
            //requestN4U.AddParameter("application/x-www-form-urlencoded", $"post={"3"}&email={"matteo.lomi@netwintec.com"}&nome={"Matteo"}&messaggio={"Test Messaggio"}", ParameterType.RequestBody);
            JObject oJsonObject = new JObject();

            //nome,cognome,email,telefono,destinazione,data_partenza,data_fine,
            //autonoleggio,giorni_autonoleggio,adulti,bambini,ragazzi,animali,tipo_alloggio.


            oJsonObject.Add("tipo", "2");
            oJsonObject.Add("nome", nome);
            oJsonObject.Add("cognome", cognome);
            oJsonObject.Add("email", email);
            oJsonObject.Add("telefono", telefono);
            oJsonObject.Add("videocallinlis", lis.ToString());
            oJsonObject.Add("whatsapp", whatsapp.ToString());
            oJsonObject.Add("destinazione", destinazione);
            oJsonObject.Add("data_partenza", data_partenza);
            oJsonObject.Add("data_fine", data_fine);
            oJsonObject.Add("autonoleggio", autonoleggio.ToString());
            oJsonObject.Add("giorni_autonoleggio", giorni_autonoleggio);
            oJsonObject.Add("adulti", adulti);
            oJsonObject.Add("bambini", bambini);
            oJsonObject.Add("ragazzi", ragazzi);
            oJsonObject.Add("animali", animali);
            oJsonObject.Add("tipo_alloggio", tipo_alloggio);

            requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);
            Console.WriteLine(client.BaseUrl + "" + requestN4U.Resource);

            client.ExecuteAsync(requestN4U, (response, arg2) => {

                Console.WriteLine("StatusCode mail_app.php:" + response.StatusCode +
                                  "\nContent mail_app.php:" + response.Content);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    InvokeOnMainThread(() =>
                    {

                        NoSend = false;
                        Toast.MakeToast("Email inviata con successo").Show();

                    });
                }
                else
                {
                    InvokeOnMainThread(() =>
                    {

                        NoSend = false;
                        Toast.MakeToast("Errore riprovare più tardi").Show();

                    });
                }

            });


        }

    }

    public class StatusPickerViewModel : UIPickerViewModel
    {

        List<string> Destinazioni;

        public event EventHandler<EventArgs> ValueChanged;

        public string SelectedItem
        {
            get { return Destinazioni[selectedIndex]; }
        }

        int selectedIndex = 0;


        public StatusPickerViewModel(List<string> d)
        {
            Destinazioni = d;
        }
		public override nint GetComponentCount(UIPickerView pickerView)
		{
            return 1;
		}

		public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
		{
            return Destinazioni.Count;
		}

		public override string GetTitle(UIPickerView pickerView, nint row, nint component)
		{
            return Destinazioni[(int)row];
            //return base.GetTitle(pickerView, row, component);
		}

		public override UIView GetView(UIPickerView pickerView, nint row, nint component, UIView view)
		{
            UILabel pickerLabel = (UILabel)view;

            if (pickerLabel == null)
            {
                pickerLabel = new UILabel();

                pickerLabel.Font = UIFont.FromName("OpenSans", 16);

                pickerLabel.TextAlignment = UITextAlignment.Center;
            }
            pickerLabel.Text = Destinazioni[(int)row];

            return pickerLabel;

            //return base.GetView(pickerView, row, component, view);
		}

        public override void Selected(UIPickerView picker, nint row, nint component)
        {
            selectedIndex = (int)row;
            ValueChanged?.Invoke(this, EventArgs.Empty);
        }


		/*
        public override int GetComponentCount(UIPickerView picker)
        {
            return 1;
        }

        public override int GetRowsInComponent(UIPickerView picker, int component)
        {
            return 5;
        }

        public override string GetTitle(UIPickerView picker, int row, int component)
        {

            return "Component " + row.ToString();
        }*/
	}
}