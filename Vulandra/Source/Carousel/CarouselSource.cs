﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using CoreGraphics;
using FFImageLoading;
using UIKit;
using Xamarin.iOS.iCarouselBinding;

namespace Vulandra
{

    public class CarouselSource : iCarouselDataSource
    {
        List<SliderObj> _data;
        nfloat Width;
        public CarouselSource(List<SliderObj> data,nfloat w)
        {
            _data = data;
            Width = w;
        }

        public override nint NumberOfItemsInCarousel(iCarousel carousel) => _data.Count;

        public override UIView ViewForItemAtIndex(iCarousel carousel, nint index, UIView view)
        {
            UIImageView imgView;

            // create new view if no view is available for recycling
            if (view == null)
            {
                imgView = new UIImageView(new CGRect(0, 0, Width, 200))
                {
                    BackgroundColor = UIColor.White,
                    ContentMode = UIViewContentMode.ScaleAspectFill,
                    ClipsToBounds = true,
                    Tag = 1
                };

                view = imgView;
            }
            else
            {
                // get a reference to the label in the recycled view
                imgView = (UIImageView)view.ViewWithTag(1);
            }

            var SliderObj = _data[(int)index];
            ImageService.Instance
                        .LoadUrl(GlobalStructureInstance.Instance.BaseImageUrl + SliderObj.media_details.file)
                        .LoadingPlaceholder("LogoDark.png")
                        .Into(imgView);
            
            return view;
        }

    }

    public class CarouselSourceDelegate : iCarouselDelegate
    {
        private readonly ViewController _viewController;

        public CarouselSourceDelegate(ViewController vc)
        {
            _viewController = vc;
        }

        public override void DidSelectItemAtIndex(iCarousel carousel, nint index)
        {
            try
            {
                if (_viewController.CorrectListSlider[(int)index].pyre_heading.Contains("VULALIS"))
                {
                    _viewController.OpenYoutubeChannel();
                }
                else if (_viewController.CorrectListSlider[(int)index].pyre_heading.Contains("OCCASIONI SPECIALI"))
                {
                    var url = "http://www.vulandraviaggi.com/2015/01/27/occasioni-speciali/";
                    _viewController.OpenUrl(url);
                }
                else if (_viewController.CorrectListSlider[(int)index].pyre_heading.Contains("4 ZAMPE"))
                {
                    var url = "http://www.vulandraviaggi.com/2015/01/27/4-zampe/";
                    _viewController.OpenUrl(url);
                }
                else if (_viewController.CorrectListSlider[(int)index].pyre_heading.Contains("SPOSAMI"))
                {
                    var url = "http://www.vulandraviaggi.com/2015/01/27/sposami/";
                    _viewController.OpenUrl(url);
                }
                else if (_viewController.CorrectListSlider[(int)index].pyre_heading.Contains("IL MONDO"))
                {
                    var storyboard = UIStoryboard.FromName("Main", null);
                    UIViewController lp = storyboard.InstantiateViewController("ViaggioIdeale");
                    _viewController.NavigationController.PushViewController(lp, true);
                }
            }catch(Exception e)
            {
                Console.WriteLine("DidSelectItemAtIndex exception trown:"+e);
            }

        }

		public override void CarouselCurrentItemIndexDidChange(iCarousel carousel)
		{
            //base.CarouselCurrentItemIndexDidChange(carousel);
            _viewController.ChangeSliderObject((int)carousel.CurrentItemIndex);
            Console.WriteLine(carousel.CurrentItemIndex);
		}
	}

            
}
