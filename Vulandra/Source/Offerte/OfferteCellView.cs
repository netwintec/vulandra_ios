﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using FFImageLoading;
using FFImageLoading.Work;

namespace Vulandra
{
    public class OfferteCellView: UITableViewCell
    {
        UILabel Titolo,Descrizione,TagLabel;
        UIImageView Image,Triangle;
        UIView separator, TagContainer;

        public OfferteCellView(NSString cellId, nfloat width, float height,OfferteObj offerteObj) : base(UITableViewCellStyle.Default, cellId)
        {

            this.SelectionStyle = UITableViewCellSelectionStyle.None;

            separator = new UIView()
            {
                BackgroundColor = UIColor.FromRGB(198, 198, 198),
            };

            TagContainer = new UIView()
            {
                BackgroundColor = UIColor.FromRGB(60, 72, 130),
            };

            // Profile Image
            Image = new UIImageView();

            ImageService.Instance
                       .LoadUrl(offerteObj.images.medium)
                       .Into(Image);
            
            Triangle = new UIImageView();
            Triangle.Image = UIImage.FromFile("RightPointTriangle.png");

            Triangle.Image = Triangle.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
            Triangle.TintColor = UIColor.White;
            Triangle.Transform = CGAffineTransform.MakeRotation((float)Math.PI);

            // name of the customer 
            Titolo = new UILabel()
            {
                Font = UIFont.FromName("OpenSans-Bold", 16f),
                TextColor = UIColor.Black,
                Lines = 1,
                Text = offerteObj.title.rendered,
                TextAlignment = UITextAlignment.Left,
                //BackgroundColor = UIColor.Red

            };

            Descrizione = new UILabel()
            {
                Font = UIFont.FromName("OpenSans", 16f),
                TextColor = UIColor.FromRGB(85,85,85),
                Lines = 2,
                Text = offerteObj.acf.descrizione,
                TextAlignment = UITextAlignment.Left,
                //BackgroundColor = UIColor.Green

            };

            TagLabel = new UILabel()
            {
                Font = UIFont.FromName("OpenSans-Bold", 14f),
                TextColor = UIColor.White,
                Lines = 1,
                Text = offerteObj.acf.categoria,
                TextAlignment = UITextAlignment.Left,
                //BackgroundColor = UIColor.Yellow

            };

            ContentView.AddSubviews(new UIView[] { separator, TagContainer, Triangle, Image, Titolo, Descrizione, TagLabel});

        }

        // layout
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            nfloat widht = ContentView.Frame.Width;
            nfloat height = ContentView.Frame.Height;

            var TagSize = UIStringDrawing.StringSize(TagLabel.Text, UIFont.FromName("OpenSans-Bold", 14f), new CGSize(120, 30));

            TagContainer.Frame = new CGRect(0, 5, TagSize.Width + 60, TagSize.Height + 10);
            TagLabel.Frame = new CGRect((TagContainer.Frame.Width - 15) / 2 - TagSize.Width / 2, 10, TagSize.Width, TagSize.Height);
            Triangle.Frame = new CGRect(TagContainer.Frame.Width - 15, 5, 15, TagContainer.Frame.Height);

            var TitSize = UIStringDrawing.StringSize(Titolo.Text, UIFont.FromName("OpenSans-Bold", 16f), new CGSize(widht-115, 2000));
            var DescSize = UIStringDrawing.StringSize(Descrizione.Text, UIFont.FromName("OpenSans", 16f), new CGSize(widht - 115, 2000));

            var totTxtH = TitSize.Height + DescSize.Height + 5;
            var yTxtStart = TagContainer.Frame.Y + TagContainer.Frame.Height + 10;
            var HTxtLay= height - (TagContainer.Frame.Y + TagContainer.Frame.Height + 20);

            Titolo.Frame = new CGRect(15, yTxtStart + (HTxtLay / 2 - totTxtH / 2) , widht - 115, TitSize.Height);
            Descrizione.Frame = new CGRect(15, Titolo.Frame.Y + Titolo.Frame.Height + 5, widht - 115, DescSize.Height);

            Image.Frame = new CGRect(widht - 85,15,70,70);

            separator.Frame = new CGRect(0, height - 1, widht, 1);

        }
    }
}

