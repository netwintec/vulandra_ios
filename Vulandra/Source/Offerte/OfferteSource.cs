﻿using System;
using System.Drawing;
using CoreGraphics;
using Foundation;
using UIKit;
using System.Collections.Generic;
using System.Linq;

namespace Vulandra
{
    public class OfferteSource: UITableViewSource
    {
        public static string LOGGER_CLASS = "OfferteSource_";

        // Event handler for item clicks:
        public event EventHandler<int> ItemClick;

        public List<OfferteObj> listOfferte;

        ViewController super;

        public OfferteSource(List<OfferteObj> lu, ViewController s)
        {
            super = s;
            listOfferte = lu;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return listOfferte.Count;
        }

        // Touch up inside, for table cell
        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            int row = indexPath.Row;
            super.ShowWebView(listOfferte[row]);
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            int row = indexPath.Row;

            nfloat height = 100;

            return height;

        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            int row = indexPath.Row;
            NSString cellIdentifier = new NSString(row.ToString());

            float rowHeight = (float)GetHeightForRow(tableView, indexPath);

            var offObj = listOfferte[row];

            OfferteCellView cellNewGroup;

            nfloat width = super.View.Frame.Width;

            cellNewGroup = new OfferteCellView(cellIdentifier, width, rowHeight,offObj);

            return cellNewGroup;


        }
    }
}
