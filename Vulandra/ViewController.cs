﻿using CoreGraphics;
using Foundation;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using UIKit;
using Xamarin.iOS.iCarouselBinding;

namespace Vulandra
{
    public partial class ViewController : UIViewController
    {
        GlobalStructureInstance GbsInstance = GlobalStructureInstance.Instance;

        UIStoryboard storyboard;

        UIImageView BackImage;
        UIMenuView menu;

        public List<SliderObj> listSlider;
        public List<SliderObj> CorrectListSlider;
        iCarousel carousel;
        UIView CarouselUnder;
        UILabel CarouselLabel;
        UIActivityIndicatorView loader;

        List<OfferteObj> listOfferte = new List<OfferteObj>();
        UITableView OfferteRecycler;
        OfferteSource offertaAdapter;
        UILabel NessunaOffertaTxt;
        UIActivityIndicatorView LoadingOfferte;

        UIView webViewBack;
        UIWebView webView;

        public static ViewController Instance { private set; get; }

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidAppear(bool animated)
        {
            try
            {
                GbsInstance.SetPage(PageName.Home);
            }
            catch (Exception) { };

            base.ViewDidAppear(animated);
        }
        
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            GbsInstance.SetPage(PageName.Home);

            ViewController.Instance = this;

            storyboard = UIStoryboard.FromName("Main", null);

            InitMenuElement();

            // nav bar settings
            base.NavigationController.NavigationBarHidden = true;
            // setstatus bar white
            UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, false);


            UINavigationBar NavigationBar = new UINavigationBar(new CGRect(0, 0, View.Frame.Width, 70));
            NavigationBar.BarTintColor = UIColor.FromRGB(102, 104, 162);
            //NavigationBar.BackgroundColor = UIColor.FromRGB(102,104,162);
            NavigationBar.Translucent = false;
            // this control is for the new nav bar, it is for matching the right dimentions.
            // you need the if for checking the ios version, it works with ios >= 11
            if (int.Parse(UIDevice.CurrentDevice.SystemVersion.Split('.')[0]) > 10)
            {
                NavigationBar.PrefersLargeTitles = true;
            }

            UIImageView MenuImage = new UIImageView(new CGRect(15, 27.5, 35, 35));
            MenuImage.Image = UIImage.FromFile("MenuClose.png");
            var imTap = new UITapGestureRecognizer(() => {
                if (menu.MenuOpen)
                {
                    menu.CloseMenu();
                }
                else
                {
                    menu.OpenMenu();
                }
            });
            MenuImage.UserInteractionEnabled = true;
            MenuImage.AddGestureRecognizer(imTap);
            NavigationBar.Add(MenuImage);


            BackImage = new UIImageView(new CGRect(View.Frame.Width -50 , 27.5, 35, 35));
            BackImage.Image = UIImage.FromFile("Back.png");
            var BackTap = new UITapGestureRecognizer(() => {
                CloseWebView();
            });
            BackImage.Alpha = 0;
            BackImage.UserInteractionEnabled = true;
            BackImage.AddGestureRecognizer(BackTap);
            NavigationBar.Add(BackImage);


            UIImageView LogoImage = new UIImageView(new CGRect(View.Frame.Width/2 - 52.5 , 27.5, 105, 35));
            LogoImage.Image = UIImage.FromFile("Logo.png");
            NavigationBar.Add(LogoImage);

            UIView Sep = new UIView(new CGRect(0, 68, View.Frame.Width, 2));
            Sep.BackgroundColor = UIColor.FromRGB(60, 72, 130);
            NavigationBar.Add(Sep);

            UIView MainView = new UIView(new CGRect(0, 70, View.Frame.Width, View.Frame.Height - 70));
            MainView.BackgroundColor = UIColor.White;

            UIView MainCarouselView = new UIView(new CGRect(0, 0, View.Frame.Width, 250));
            MainCarouselView.BackgroundColor = UIColor.White;
            MainView.Add(MainCarouselView);

            loader = new UIActivityIndicatorView(new CGRect(View.Frame.Width/2 - 20, MainCarouselView.Frame.Height/2 - 20, 40, 40));
            loader.Color = UIColor.FromRGB(102, 104, 162);
            loader.StartAnimating();
            MainCarouselView.Add(loader);

            carousel = new iCarousel
            {
                ContentMode = UIViewContentMode.Center,
                Type = iCarouselType.Linear,
                Frame = View.Frame,
                CenterItemWhenSelected = true,
                ClipsToBounds = true,
                Alpha = 0,
            };
            carousel.Frame = new CGRect(0, 0, View.Frame.Width, 200);
            carousel.Bounces = false;
            MainCarouselView.Add(carousel);

            CarouselUnder = new UIView(new CGRect(0, 200, View.Frame.Width, 50));
            CarouselUnder.BackgroundColor = UIColor.FromRGB(60, 72, 130);
            CarouselUnder.Alpha = 0;

            CarouselLabel = new UILabel(new CGRect(15, 15, View.Frame.Width - 30, 20));
            CarouselLabel.TextColor = UIColor.White;
            CarouselLabel.TextAlignment = UITextAlignment.Center;
            CarouselLabel.Font = UIFont.FromName("OpenSans", 14);
            CarouselLabel.Lines = 1;
            CarouselLabel.Text = "";
            CarouselUnder.Add(CarouselLabel);

            MainCarouselView.Add(CarouselUnder);

            UIView ProposteView = new UIView();
            ProposteView.BackgroundColor = UIColor.FromRGB(226, 105, 39);
            MainView.Add(ProposteView);
            
            string PrTxt = NSBundle.MainBundle.LocalizedString("Home_NostreProposte", "Le nostre proposte:", null);

            var PrSize = UIStringDrawing.StringSize(PrTxt, UIFont.FromName("Montserrat-Bold",18), new CGSize(View.Frame.Width - 30, 40));

            // Serve font monserratbold
            UILabel ProposteTxt = new UILabel(new CGRect( View.Frame.Width /2 - PrSize.Width/2, 10, PrSize.Width, PrSize.Height));
            ProposteTxt.TextColor = UIColor.White;
            ProposteTxt.Font = UIFont.FromName("Montserrat-Bold",18);
            ProposteTxt.Text = PrTxt;
            ProposteView.Add(ProposteTxt);

            ProposteView.Frame = new CGRect(0, 250, View.Frame.Width, ProposteTxt.Frame.Height + 20);


            UIView OfferteView = new UIView(new CGRect(0, ProposteView.Frame.Y+ ProposteView.Frame.Height, 
                                                       View.Frame.Width, MainView.Frame.Height - (ProposteView.Frame.Y + ProposteView.Frame.Height)));
            OfferteView.BackgroundColor = UIColor.White;
            MainView.Add(OfferteView);

            LoadingOfferte = new UIActivityIndicatorView(new CGRect(OfferteView.Frame.Width / 2 - 20, OfferteView.Frame.Height / 2 - 20, 40, 40));
            LoadingOfferte.Color = UIColor.FromRGB(102, 104, 162);
            LoadingOfferte.StartAnimating();
            OfferteView.Add(LoadingOfferte);

            string OffTxt = NSBundle.MainBundle.LocalizedString("Home_NessunaOffeta", "", null);

            var OffSize = UIStringDrawing.StringSize(OffTxt, UIFont.FromName("Montserrat-Bold",14), new CGSize(View.Frame.Width - 30, 40));

			NessunaOffertaTxt= new UILabel(new CGRect(OfferteView.Frame.Width / 2 - OffSize.Width / 2, OfferteView.Frame.Height / 2 - OffSize.Height / 2, OffSize.Width, OffSize.Height));
            NessunaOffertaTxt.TextColor = UIColor.Black;
            NessunaOffertaTxt.Font = UIFont.FromName("Montserrat-Bold",14);
            NessunaOffertaTxt.Text = OffTxt;
            NessunaOffertaTxt.Alpha = 0;
            OfferteView.Add(NessunaOffertaTxt);

            OfferteRecycler = new UITableView(new CGRect(0,0,OfferteView.Frame.Width, OfferteView.Frame.Height));
            OfferteRecycler.Alpha = 0;
            OfferteView.Add(OfferteRecycler);

            offertaAdapter = new OfferteSource(listOfferte,this);
            OfferteRecycler.Source = offertaAdapter;

            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                DownloadSliderData(0);
                DownloadDestinationData(0);
                DownloadOfferteData(0);
            })).Start();

            webViewBack = new UIView(new CGRect(
                    View.Frame.Width,
                    70,
                    View.Frame.Width,
                    View.Frame.Height - 70));
            webViewBack.BackgroundColor = UIColor.White;

            UIActivityIndicatorView loaderWeb = new UIActivityIndicatorView(new CGRect(webViewBack.Frame.Width / 2 - 25, webViewBack.Frame.Height / 2 - 25, 50, 50));
            loaderWeb.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge;
            loaderWeb.Color = UIColor.FromRGB(28, 151, 219);
            loaderWeb.StartAnimating();
            webViewBack.Add(loaderWeb);

            webView = new UIWebView(new CGRect(
                    0,
                    0,
                    webViewBack.Frame.Width,
                    webViewBack.Frame.Height));
            webView.Alpha = 0;
            webView.LoadFinished += delegate
            {

                webView.Alpha = 1;

            };
            webViewBack.Add(webView);

            View.Add(NavigationBar);
            View.Add(MainView);
            View.Add(webViewBack);

            // INIT MENU

            menu = new UIMenuView(new CGRect(0, 70, View.Frame.Width, View.Frame.Height - 70));
            menu.SetBackgroundColor(UIColor.FromRGBA(0, 0, 0, 75));
            menu.SetMenuColor(UIColor.FromRGB(102, 104, 162));
            menu.PAGE_TAG = "0";
            menu.CellHeight = 50;
            menu.CellWidht = 200;
            menu.MenuImg = MenuImage;

            menu.AddElement(GbsInstance.ListMenuElement);

            View.Add(menu);
        }

        public void CloseWebView()
        {
            BackImage.Alpha = 0;

            UIView.Animate(0.4, () =>
            {
                InvokeOnMainThread(() => {

                    webViewBack.Frame = new CGRect(View.Frame.Width, 70, View.Frame.Width, View.Frame.Height - 70);
                });
            });
        }


        public void ShowWebView(OfferteObj offObj)
        {
            webView.Alpha = 0;
            BackImage.Alpha = 1;

            UIView.Animate(0.4, () =>
             {
                InvokeOnMainThread(() => {
             
                    webViewBack.Frame = new CGRect(0,70,View.Frame.Width,View.Frame.Height - 70);
                });
             });

            var url = offObj.acf.link;
            if (url.EndsWith(".pdf", StringComparison.CurrentCulture))
            {
                //url = "offObj.acf.link";
                webView.LoadRequest(new NSUrlRequest(new NSUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + url)));
            }
            else
            {
                webView.LoadRequest(new NSUrlRequest(new NSUrl(url)));
            }

        }

        public void DownloadSliderData(int time)
        {

            var client = new RestClient(GbsInstance.BaseUrl);

            var requestN4U = new RestRequest("wp-json/wp/v2/slide", Method.GET);

            Console.WriteLine(client.BaseUrl + "" + requestN4U.Resource);

            IRestResponse response = client.Execute(requestN4U);

            Console.WriteLine("StatusCode v2/slide:" + response.StatusCode +
                                              "\nContent v2/slide:" + response.Content);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //var StrResponse = response.Content.Substring(1, response.Content.Length - 2);
                listSlider = JsonConvert.DeserializeObject<List<SliderObj>>(response.Content);
            }
            else
            {
                listSlider = new List<SliderObj>();
            }

            foreach (var obj in listSlider)
            {

                if (obj.pyre_button_1 != "")
                {
                    if (obj.pyre_heading.Contains("<img src"))
                        obj.pyre_heading = "VULALIS";


                    var client2 = new RestClient(GbsInstance.BaseUrl);

                    //Console.WriteLine(GbsInstance.BaseUrl+"wp-json/wp/v2/media/"+obj.featured_media);

                    var requestN4U2 = new RestRequest("wp-json/wp/v2/media/" + obj.featured_media, Method.GET);

                    IRestResponse response2 = client2.Execute(requestN4U2);

                    Console.WriteLine("StatusCode v2/media:" + response2.StatusCode +
                                                                  "\nContent v2/media:" + response2.Content);

                    if (response2.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        //var StrResponse = response2.Content.Substring(1, response2.Content.Length - 2);
                        var Slobj = JsonConvert.DeserializeObject<SliderObj>(response2.Content);
                        obj.media_details = Slobj.media_details;
                    }
                    else
                    {
                        var md = new MediaDetails();
                        md.file = "";
                        obj.media_details = md;
                    }

                    Console.WriteLine("SLOBJ:" + obj.pyre_heading + "|" + obj.pyre_caption + "|" + obj.featured_media + "|" + obj.media_details.file);
                }
            }


            InvokeOnMainThread(() =>
            {


                CorrectListSlider = new List<SliderObj>();
                foreach (var obj in listSlider)
                {
                    if (obj.pyre_button_1 != "")
                    {
                        CorrectListSlider.Add(obj);
                    }
                }

                var source = new CarouselSource(CorrectListSlider, View.Frame.Width);
                var delagate = new CarouselSourceDelegate(this);

                carousel.DataSource = source;
                carousel.Delegate = delagate;

                carousel.CurrentItemIndex = 0;

                loader.Alpha = 0;
                carousel.Alpha = 1;
                CarouselUnder.Alpha = 1;

                ChangeSliderObject(0);

                carousel.ReloadData();

                //cambiaPallino();
                return;
            });

        }

        public void DownloadDestinationData(int time)
        {

            var client = new RestClient(GbsInstance.BaseUrl);

            var requestN4U = new RestRequest("wp-json/wp/v2/avada_portfolio", Method.GET);

            Console.WriteLine(client.BaseUrl + "" + requestN4U.Resource);

            IRestResponse response = client.Execute(requestN4U);

            Console.WriteLine("StatusCode v2/avada_portfolio:" + response.StatusCode +
                                              "\nContent v2/avada_portfolio:" + response.Content);

            var list = new List<DestinazioniObj>();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //var StrResponse = response.Content.Substring(1, response.Content.Length - 2);
                list = JsonConvert.DeserializeObject<List<DestinazioniObj>>(response.Content);
            }

            foreach (var obj in list)
            {
                GbsInstance.ListDestinazioni.Add(obj.title.rendered);
            }
        }

        public void DownloadOfferteData(int time)
        {

            var client = new RestClient(GbsInstance.BaseUrl);

            var requestN4U = new RestRequest("wp-json/wp/v2/posts", Method.GET);
            requestN4U.AddQueryParameter("categories", "45");
            Console.WriteLine(client.BaseUrl + "" + requestN4U.Resource);

            IRestResponse response = client.Execute(requestN4U);

            Console.WriteLine("StatusCode v2/posts?categories=45:" + response.StatusCode +
                              "\nContent v2/posts?categories=45:" + response.Content);

            var list = new List<OfferteObj>();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //var StrResponse = response.Content.Substring(1, response.Content.Length - 2);
                list = JsonConvert.DeserializeObject<List<OfferteObj>>(response.Content);
                InvokeOnMainThread(() =>
                {
                    foreach (var v in list)
                        listOfferte.Add(v);
                    
                    OfferteRecycler.ReloadData();

                    if (listOfferte.Count == 0)
                    {
                        OfferteRecycler.Alpha = 0;
                        LoadingOfferte.Alpha = 0;
                        NessunaOffertaTxt.Alpha = 1;
                    }
                    else
                    {
                        OfferteRecycler.Alpha = 1;
                        LoadingOfferte.Alpha = 0;
                        NessunaOffertaTxt.Alpha = 0;
                    }
                });

            }
            else
            {
				InvokeOnMainThread(() =>
				{
					OfferteRecycler.Alpha = 0;
					LoadingOfferte.Alpha = 0;
					NessunaOffertaTxt.Alpha = 1;
				});
            }
        }

        public void ChangeSliderObject(int position)
        {

            CarouselLabel.Text = CorrectListSlider[position].pyre_heading;

        }

        public void OpenYoutubeChannel()
        {

            var url = "https://www.youtube.com/channel/UCiUlYAQvsa-v0_-5Y9uwuYw";
            UIApplication.SharedApplication.OpenUrl(NSUrl.FromString(url));
            /*
            try
            {
                intent = new Intent(Intent.ActionView);
                intent.SetPackage("com.google.android.youtube");
                intent.SetData(Android.Net.Uri.Parse(url));
                StartActivity(intent);
            }
            catch (ActivityNotFoundException ee)
            {
                intent = new Intent(Intent.ActionView);
                intent.SetData(Android.Net.Uri.Parse(url));
                StartActivity(intent);
            }
            */
        }

        public void OpenUrl(string url)
        {

            UIApplication.SharedApplication.OpenUrl(NSUrl.FromString(url));

        }

        private void InitMenuElement() // Serve font monserratbold
        {
            //****************************

            string CStxt = NSBundle.MainBundle.LocalizedString("Menu_CS", "Chi Siamo", null);

            MenuElement CS = new MenuElement(CStxt, UIColor.White, "Montserrat-Bold", "OrangeCircle.png", "1");
            CS.TextColor = UIColor.White;
            CS.Click += (sender, e) =>
            {
                Console.WriteLine(CStxt);
                UIViewController lp = storyboard.InstantiateViewController("ChiSiamo");
                this.NavigationController.PushViewController(lp, true);

            };

            GbsInstance.ListMenuElement.Add(CS);

            //*********************************

            string VStxt = NSBundle.MainBundle.LocalizedString("Menu_VS", "Viaggiare Sicuri", null);

            MenuElement VS = new MenuElement(VStxt, UIColor.White, "Montserrat-Bold", "GreenCircle.png", "2");
            VS.TextColor = UIColor.White;
            VS.Click += (sender, e) =>
            {
                Console.WriteLine(VStxt);

                UIViewController lp = storyboard.InstantiateViewController("ViaggiareSicuri");
                this.NavigationController.PushViewController(lp, true);

            };

            GbsInstance.ListMenuElement.Add(VS);

            //*********************************

            string VItxt = NSBundle.MainBundle.LocalizedString("Menu_VI", "Viaggio Ideale", null);

            MenuElement VI = new MenuElement(VItxt, UIColor.White, "Montserrat-Bold", "YellowCircle.png", "3");
            VI.TextColor = UIColor.White;
            VI.Click += (sender, e) =>
            {
                Console.WriteLine(VItxt);

                UIViewController lp = storyboard.InstantiateViewController("ViaggioIdeale");
                this.NavigationController.PushViewController(lp, true);
            };

            GbsInstance.ListMenuElement.Add(VI);

            //*********************************

            string TVtxt = NSBundle.MainBundle.LocalizedString("Menu_TV", "Il tuo viaggio", null);

            MenuElement TV = new MenuElement(TVtxt, UIColor.White, "Montserrat-Bold", "PurpleCircle.png", "4");
            TV.TextColor = UIColor.White;
            TV.Click += (sender, e) =>
            {
                Console.WriteLine(TVtxt);

                UIViewController lp = storyboard.InstantiateViewController("TuoViaggio");
                this.NavigationController.PushViewController(lp, true);

                /*var intent = new Intent(this, typeof(IlTuoViaggioActivity));
                FinishActivityOnMenuChange();
                StartActivity(intent);*/
            };

            GbsInstance.ListMenuElement.Add(TV);

            //*********************************

            string Ctxt = NSBundle.MainBundle.LocalizedString("Menu_C", "Contatti", null);

            MenuElement C = new MenuElement(Ctxt, UIColor.White, "Montserrat-Bold", "RedCircle.png", "5");
            C.TextColor = UIColor.White;
            C.Click += (sender, e) =>
            {
                Console.WriteLine(Ctxt);

                UIViewController lp = storyboard.InstantiateViewController("Contatti");
                this.NavigationController.PushViewController(lp, true);

                /*var intent = new Intent(this, typeof(ContattiActivity));
                FinishActivityOnMenuChange();
                StartActivity(intent);*/
            };

            GbsInstance.ListMenuElement.Add(C);

            //*********************************

            //*********************************

            string Vtxt = NSBundle.MainBundle.LocalizedString("Menu_V", "Vulalis", null);

            MenuElement V = new MenuElement(Vtxt, UIColor.White, "Montserrat-Bold", "CyanCircle.png", "6");
            V.TextColor = UIColor.White;
            V.Click += (sender, e) =>
            {
                Console.WriteLine(Ctxt);

                OpenYoutubeChannel();
            };

            GbsInstance.ListMenuElement.Add(V);

            //*********************************

        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}