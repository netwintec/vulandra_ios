﻿using System;
using UIKit;

namespace Vulandra
{
	public class MenuElement
	{
		public string Text;
		public string ImgPath;
		public UIColor TextColor;
		public string TextFont;
		public string TAG;
		public EventHandler Click;

		public MenuElement()
		{
			
		}

		public MenuElement(string text, string path, string tag)
		{
			Text = text;
            ImgPath = path;
			TAG = tag;
		}

		public MenuElement(string text, UIColor textColor,string textFont, string path, string tag)
		{
			Text = text;
			TextColor = textColor;
			TextFont = textFont;
            ImgPath = path;
			TAG = tag;
		}
	}
}
