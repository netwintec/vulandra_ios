﻿using System;
using System.Drawing;

using CoreGraphics;
using Foundation;
using UIKit;
using System.Collections.Generic;

namespace Vulandra
{
    [Register("UIMenuView")]
    public class UIMenuView : UIView
    {
        public string PAGE_TAG;

        public UIView Background;
        UIColor BackgroundColor;

        public UIView Menu;
        UIColor MenuColor;
        bool IsRight;
        int StartX;

        public UIScrollView ScrollMenu;

        public int CellHeight;
        public int CellWidht;
        
        public bool MenuOpen { get; private set; }
        public UIImageView MenuImg;

        List<MenuElement> ListElement = new List<MenuElement>();

        public UIMenuView()
        {
            Initialize();
        }

        public UIMenuView(RectangleF bounds) : base(bounds)
        {
            Initialize();
        }
        public UIMenuView(CGRect Rect) : base(Rect)
        {
            Initialize();
        }

        void Initialize()
        {

            BackgroundColor = UIColor.White;
            MenuColor = UIColor.Blue;
            IsRight = false;
            StartX = 0;
            MenuOpen = false;
            CellHeight = 50;
            CellWidht = 200;


            this.Alpha = 0;
            this.ClipsToBounds = true;

            Background = new UIView(new CGRect(0,0,this.Frame.Width,this.Frame.Height));
            Background.BackgroundColor = BackgroundColor;
            this.Add(Background);

            var BackgroundTap = new UITapGestureRecognizer(() => { CloseMenu(); });
            Background.UserInteractionEnabled = true;
            Background.AddGestureRecognizer(BackgroundTap);

            Menu = new UIView(new CGRect(StartX, 0, 0, 0));
            Menu.BackgroundColor=MenuColor;
            Menu.ClipsToBounds = true;
            this.Add(Menu);
            

            ScrollMenu = new UIScrollView(new CGRect(0, 0, CellWidht, Menu.Frame.Height));
            ScrollMenu.DelaysContentTouches = false;
            Menu.Add(ScrollMenu);
            
        }

        public void AddElement(MenuElement me)
        {

            CreateView(me);

        }

        public void AddElement(List<MenuElement> ListMe)
        {
            foreach (var me in ListMe)
            {
                CreateView(me);
            }
        }

        private void CreateView(MenuElement me)
        {
            int add = 0;
            if (ScrollMenu.Subviews.Length == 0)
                add = 10;
            UIView view = new UIView(new CGRect(0, ScrollMenu.Frame.Height + add, CellWidht,CellHeight));
            view.BackgroundColor = UIColor.Clear;
            ScrollMenu.Add(view);


            int ImgDim = 15;

            UIImageView img = new UIImageView(new CGRect(15,CellHeight/2 - ImgDim/2,ImgDim,ImgDim));
            img.Image = UIImage.FromFile(me.ImgPath);
            view.Add(img);

            CGSize SizeLabel = new CGSize(0,0);
            if (me.TextFont != null)
                SizeLabel = UIStringDrawing.StringSize(me.Text, UIFont.FromName(me.TextFont, 18), new CGSize(CellWidht - 40 -ImgDim, 40));
            else
                SizeLabel = UIStringDrawing.StringSize(me.Text, UIFont.SystemFontOfSize(18), new CGSize(CellWidht - 40 - ImgDim, 40));

            UILabel txt = new UILabel(new CGRect(15 + ImgDim + 10,CellHeight/2-SizeLabel.Height/2,SizeLabel.Width,SizeLabel.Height));
            txt.Text = me.Text;
            if (me.TextFont != null)
                txt.Font = UIFont.FromName(me.TextFont, 18);
            else
                txt.Font = UIFont.SystemFontOfSize(18);

            if (me.TextColor != null)
                txt.TextColor = me.TextColor;
            else
                txt.TextColor = UIColor.Black;
            view.Add(txt);


            UIView sep = new UIView(new CGRect(0,CellHeight-1,CellWidht,1));
            sep.BackgroundColor = UIColor.White;
            view.Add(sep);

            UIButton Tap = new UIButton(new CGRect(0, 0, CellWidht, CellHeight));
            Tap.SetTitle(me.TAG, UIControlState.Normal);
            Tap.SetTitleColor(UIColor.Clear, UIControlState.Normal);
            Tap.TouchUpInside += (sender, e) => {

                UIButton btn = sender as UIButton;

                if (btn.Title(UIControlState.Normal) == PAGE_TAG)
                {
                    CloseMenu();
                }
                else
                {
                    if (me.Click != null)
                        me.Click(this, new EventArgs());

                    CloseMenu();
                }

            };
            view.Add(Tap);

            ListElement.Add(me);

            /*var MenuFr = Menu.Frame;
            MenuFr.Height = CellHeight * ListElement.Count + 20;
            if (MenuFr.Height > this.Frame.Height)
                MenuFr.Height = this.Frame.Height;
            Menu.Frame = MenuFr;*/

            var MenuScr = ScrollMenu.Frame;
            MenuScr.Height = CellHeight * ListElement.Count + 20;
            if (MenuScr.Height > this.Frame.Height)
                MenuScr.Height = this.Frame.Height;
            ScrollMenu.Frame = MenuScr;

            ScrollMenu.ContentSize = new CGSize(CellWidht, CellHeight * ListElement.Count);

        }

        public void OpenMenu()
        {

            if (!MenuOpen)
            {
                this.Alpha = 1;

                if (MenuImg != null)
                    MenuImg.Image = UIImage.FromFile("MenuOpen.png");

                MenuOpen = true;

                UIView.Animate(0.3,
                    () => {

                        Menu.Frame = new CGRect(StartX, 0, CellWidht, CellHeight * ListElement.Count + 20);
                    },
                    () => {
                    });
            }
        }

        public void CloseMenu()
        {

            if (MenuOpen)
            {
                MenuOpen = false;

                UIView.Animate(0.3,
                    () => {

                        Menu.Frame = new CGRect(StartX, 0, 0, 0);
                    },
                    () => {

                        if (!MenuOpen)
                        {
                            if (MenuImg != null)
                                MenuImg.Image = UIImage.FromFile("MenuClose.png");

                            this.Alpha = 0;
                            ScrollMenu.ScrollRectToVisible(new CGRect(0, 0,1,1),true);
                        }

                    });

            }
        }

        /*MenuValueAnimator = ValueAnimator.OfInt(0);
            MenuValueAnimator.SetDuration(300);
            MenuValueAnimator.Update += (sender, e) =>
            {

                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)ScrollMenu.LayoutParameters;
                layoutParams.Height = PixelsToDp(((Menu.ChildCount * CellHeight) + 20) * ((float)e.Animation.AnimatedValue / 100f));
                layoutParams.Width = PixelsToDp(CellWidht * ((float)e.Animation.AnimatedValue / 100f));
                ScrollMenu.LayoutParameters = layoutParams;

            };
            MenuValueAnimator.AnimationEnd += (sender, e) =>
            {

                if (!MenuOpen)
                {
                    if (MenuImg != null)
                        MenuImg.SetImageResource(Resource.Drawable.MenuClose);

                    this.Visibility = ViewStates.Gone;
                    ScrollMenu.ScrollTo(0, 0);
                }
            };*/

        public void SetRightMenu(bool isRight)
        {
            IsRight = isRight;

            if (IsRight)
            {
                StartX =(int) this.Frame.Width - CellWidht;

                var MenuFr = Menu.Frame;
                MenuFr.X = StartX;
                Menu.Frame = MenuFr;
            }
            else
            {
                StartX = 0;

                var MenuFr = Menu.Frame;
                MenuFr.X = StartX;
                Menu.Frame = MenuFr;
            }
        }

        public void SetBackgroundColor(UIColor c)
        {
            BackgroundColor = c;
            Background.BackgroundColor = (BackgroundColor);
        }

        public void SetMenuColor(UIColor c)
        {
            MenuColor = c;
            Menu.BackgroundColor = (MenuColor);
        }
    }
}


