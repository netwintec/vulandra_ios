﻿using CoreGraphics;
using CoreLocation;
using Foundation;
using Google.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using GlobalToast;
using RestSharp;
using System.Net.Mail;
using Newtonsoft.Json.Linq;

namespace Vulandra
{
    public partial class ViaggioIdealePage : UIViewController
    {
        GlobalStructureInstance GbsInstance = GlobalStructureInstance.Instance;

        UIMenuView menu;

        MapView map;
        public Dictionary<string, MarkerInfo> MarkerInfoDictionary = new Dictionary<string, MarkerInfo>();
        public List<Marker> MarkerList = new List<Marker>();
        int HueGiallo = 49, HueViola = 290, HueRosso = 352, HueVerde = 141, HueArancione = 19, HueBlu = 228;

        UIColor Giallo = UIColor.FromRGB(226, 188, 0);
        UIColor Viola = UIColor.FromRGB(134, 63, 145);
        UIColor Rosso = UIColor.FromRGB(220, 12, 33);
        UIColor Verde = UIColor.FromRGB(44, 169, 91);
        UIColor Arancione = UIColor.FromRGB(226, 105, 39);
        UIColor Blu = UIColor.FromRGB(60, 72, 130);

        string ModalMarkerTitle;
        UIView ModalBackground;
        bool ModalSendEnabled;
        DateTime DaDate, ADate;
        bool DaOpen = false, AOpen = false;

        public static ViaggioIdealePage Instance { private set; get; }


        public ViaggioIdealePage(IntPtr handle) : base(handle)
        {
        }


        public override void ViewDidAppear(bool animated)
        {
            try
            {
                GbsInstance.SetPage(PageName.ChiSiamo);
            }
            catch (Exception) { };

            base.ViewDidAppear(animated);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            GbsInstance.SetPage(PageName.ChiSiamo);

            ViaggioIdealePage.Instance = this;

            UIView NavigationBar = new UIView(new CGRect(0, 0, View.Frame.Width, 70));
            NavigationBar.BackgroundColor = UIColor.FromRGB(102, 104, 162);

            UIImageView MenuImage = new UIImageView(new CGRect(15, 27.5, 35, 35));
            MenuImage.Image = UIImage.FromFile("MenuClose.png");
            var imTap = new UITapGestureRecognizer(() => {
                if (menu.MenuOpen)
                {
                    menu.CloseMenu();
                }
                else
                {
                    menu.OpenMenu();
                }
            });
            MenuImage.UserInteractionEnabled = true;
            MenuImage.AddGestureRecognizer(imTap);
            NavigationBar.Add(MenuImage);

            UIImageView BackImage = new UIImageView(new CGRect(View.Frame.Width - 50, 27.5, 35, 35));
            BackImage.Image = UIImage.FromFile("Back.png");
            var backTap = new UITapGestureRecognizer(() => {
                this.NavigationController.PopToRootViewController(true);
            });
            BackImage.UserInteractionEnabled = true;
            BackImage.AddGestureRecognizer(backTap);
            NavigationBar.Add(BackImage);

            UIImageView LogoImage = new UIImageView(new CGRect(View.Frame.Width / 2 - 52.5, 27.5, 105, 35));
            LogoImage.Image = UIImage.FromFile("Logo.png");
            NavigationBar.Add(LogoImage);

            UIView Sep = new UIView(new CGRect(0, 68, View.Frame.Width, 2));
            Sep.BackgroundColor = UIColor.FromRGB(226, 188, 0);
            NavigationBar.Add(Sep);

            UIView TitleView = new UIView(new CGRect(0, 70, View.Frame.Width, 110));
			TitleView.BackgroundColor = UIColor.FromRGB(226, 188, 0) ;

            string Titletxt = NSBundle.MainBundle.LocalizedString("ViaggioIdeale_Tit", "", null);
            var TitleSize = UIStringDrawing.StringSize(Titletxt, UIFont.FromName("OpenSans-Bold", 18), new CGSize(View.Frame.Width - 30, 80));

            UILabel TitleLabel = new UILabel(new CGRect(15, 60 / 2 - TitleSize.Height / 2, View.Frame.Width - 30, TitleSize.Height));
            TitleLabel.TextColor = UIColor.White;
            TitleLabel.TextAlignment = UITextAlignment.Center;
            TitleLabel.Font = UIFont.FromName("OpenSans-Bold", 18);
            TitleLabel.Lines = 0;
            TitleLabel.Text = Titletxt;
            TitleView.Add(TitleLabel);

            UIView SepTitle = new UIView(new CGRect(0, 60, View.Frame.Width, 50));
            SepTitle.BackgroundColor = UIColor.FromRGB(226, 188, 0);
            TitleView.Add(SepTitle);

            string Descrtxt = NSBundle.MainBundle.LocalizedString("ViaggioIdeale_Descr", "", null);
            var DescrSize = UIStringDrawing.StringSize(Descrtxt, UIFont.FromName("OpenSans", 14), new CGSize(View.Frame.Width - 30, 50));

            UILabel DescrLabel = new UILabel(new CGRect(15, 25 - DescrSize.Height/2, View.Frame.Width - 30, DescrSize.Height));
            DescrLabel.TextColor = UIColor.White;
            DescrLabel.TextAlignment = UITextAlignment.Center;
            DescrLabel.Font = UIFont.FromName("OpenSans", 14);
            DescrLabel.Lines = 0;
            DescrLabel.Text = Descrtxt;
            SepTitle.Add(DescrLabel);

            UIView MainView = new UIView(new CGRect(0, 180, View.Frame.Width, View.Frame.Height - 180));
            MainView.BackgroundColor = UIColor.White;

            // INIZIALIZZAZIONE MARKER

            // ARANCIONE
            MarkerInfoDictionary.Add("m0", new MarkerInfo(59.948807, -109.602792, "CANADA", "maggio - settembre", HueArancione));
            MarkerInfoDictionary.Add("m1", new MarkerInfo(40.713390, -73.990562, "NEW YORK", "evergreen", HueArancione));
            MarkerInfoDictionary.Add("m2", new MarkerInfo(35.932960, -120.891827, "LOS ANGELES E SAN FRANCISCO", "aprile - settembre", HueArancione));
            MarkerInfoDictionary.Add("m3", new MarkerInfo(25.788992, -80.205249, "MIAMI", "novembre - aprile", HueArancione));
            MarkerInfoDictionary.Add("m4", new MarkerInfo(20.486772, -157.244795, "HAWAII", "maggio - settembre", HueArancione));

            // ROSSO
            MarkerInfoDictionary.Add("m5", new MarkerInfo(21.659385, -78.105960, "CUBA", "novembre - aprile", HueRosso));
            MarkerInfoDictionary.Add("m6", new MarkerInfo(23.696244, -102.821168, "MESSICO", "novembre - aprile", HueRosso));
            MarkerInfoDictionary.Add("m7", new MarkerInfo(18.538669, -69.910145, "SANTO DOMINGO", "novembre - aprile", HueRosso));
            MarkerInfoDictionary.Add("m8", new MarkerInfo(11.343386, -61.714891, "ANTILLE", "dicembre - aprile", HueRosso));
            MarkerInfoDictionary.Add("m9", new MarkerInfo(11.843107, -66.736363, "LOS ROQUES\n(Venezuela)", "dicembre - aprile", HueRosso));
            MarkerInfoDictionary.Add("m10", new MarkerInfo(-9.309048, -53.196412, "BRASILE", "ottobre - marzo", HueRosso));
            MarkerInfoDictionary.Add("m11", new MarkerInfo(-36.058717, -65.501099, "ARGENTINA", "novembre - aprile", HueRosso));

            // VERDE
            MarkerInfoDictionary.Add("m12", new MarkerInfo(64.820631, -18.494737, "ISLANDA", "giugno - settembre", HueVerde));
            MarkerInfoDictionary.Add("m13", new MarkerInfo(65.543632, 23.318341, "EUROPA DEL NORD", "giugno - settembre", HueVerde));
            MarkerInfoDictionary.Add("m14", new MarkerInfo(42.511424, 12.512715, "MEDITERRANEO", "maggio - ottobre", HueVerde));

            // GIALLO
            MarkerInfoDictionary.Add("m15", new MarkerInfo(32.210394, -6.749489, "MAROCCO", "aprile - ottobre", HueGiallo));
            MarkerInfoDictionary.Add("m16", new MarkerInfo(33.656139, 9.537253, "TUNISIA", "aprile - ottobre", HueGiallo));
            MarkerInfoDictionary.Add("m17", new MarkerInfo(15.729612, -24.192744, "CAPO VERDE", "novembre - luglio", HueGiallo));
            MarkerInfoDictionary.Add("m18", new MarkerInfo(26.218508, 28.861057, "EGITTO", "aprile - ottobre", HueGiallo));
            MarkerInfoDictionary.Add("m19", new MarkerInfo(0.472157, 36.742038, "KENYA", "dicembre - marzo", HueGiallo));
            MarkerInfoDictionary.Add("m20", new MarkerInfo(-6.195318, 37.072609, "ZANZIBAR E TANZANIA", "dicembre - marzo", HueGiallo));
            MarkerInfoDictionary.Add("m21", new MarkerInfo(-4.673900, 55.453885, "SEYCHELLES", "aprile - ottobre", HueGiallo));
            MarkerInfoDictionary.Add("m22", new MarkerInfo(-20.264646, 57.592525, "MAURITIUS", "aprile - ottobre", HueGiallo));
            MarkerInfoDictionary.Add("m23", new MarkerInfo(-31.330209, 24.016350, "SUD AFRICA", "ottobre - aprile", HueGiallo));
            MarkerInfoDictionary.Add("m24", new MarkerInfo(-20.599683, 46.100124, "MADAGASCAR", "aprile - ottobre", HueGiallo));

            // BLU
            MarkerInfoDictionary.Add("m25", new MarkerInfo(63.322945, 92.993800, "RUSSIA", "maggio - settembre", HueBlu));
            MarkerInfoDictionary.Add("m26", new MarkerInfo(36.782442, 138.925245, "GIAPPONE", "dicembre - aprile", HueBlu));
            MarkerInfoDictionary.Add("m27", new MarkerInfo(35.417176, 104.361449, "CINA", "aprile - settembre", HueBlu));
            MarkerInfoDictionary.Add("m28", new MarkerInfo(25.102363, 55.153171, "DUBAI", "novembre - maggio", HueBlu));
            MarkerInfoDictionary.Add("m29", new MarkerInfo(21.815329, 96.689845, "BIRMANIA", "novembre - marzo", HueBlu));
            MarkerInfoDictionary.Add("m30", new MarkerInfo(17.327963, 74.988953, "INDIA E LACCADIVE", "novembre - aprile", HueBlu));
            MarkerInfoDictionary.Add("m31", new MarkerInfo(-2.846645, 121.481105, "INDONESIA", "aprile - ottobre", HueBlu));
            MarkerInfoDictionary.Add("m32", new MarkerInfo(2.382135, 76.813779, "SRI LANKA E MALDIVE", "gennaio - aprile", HueBlu));
            MarkerInfoDictionary.Add("m33", new MarkerInfo(15.947488, 100.469351, "THAILANDIA", "novembre - luglio", HueBlu));
            MarkerInfoDictionary.Add("m34", new MarkerInfo(4.638732, 102.139274, "MALESIA", "novembre - luglio", HueBlu));

            // VIOLA
            MarkerInfoDictionary.Add("m35", new MarkerInfo(-6.678904, 144.502549, "PAPUA NUOVA GUINEA", "maggio - ottobre", HueViola));
            MarkerInfoDictionary.Add("m36", new MarkerInfo(-17.365227, 133.646709, "AUSTRALIA NORD", "maggio - ottobre", HueViola));
            MarkerInfoDictionary.Add("m37", new MarkerInfo(-31.465597, 135.091100, "AUSTRALIA SUD", "novembre - maggio", HueViola));
            MarkerInfoDictionary.Add("m38", new MarkerInfo(-13.922528, -166.641066, "POLINESIA", "aprile - ottobre", HueViola));
            MarkerInfoDictionary.Add("m39", new MarkerInfo(-17.744841, 178.171009, "ISOLE FUJI", "maggio - ottobre", HueViola));
            MarkerInfoDictionary.Add("m40", new MarkerInfo(-43.091808, 171.844557, "NUOVA ZELANDA", "novembre - maggio", HueViola));


            //MapView map = new MapView(new CGRect(0, 0, MainView.Frame.Width, MainView.Frame.Height));

            var camera = CameraPosition.FromCamera(latitude: 0,
                                            longitude: 0,
                                            zoom: 0);
            map = MapView.FromCamera(new CGRect(0, 0, MainView.Frame.Width, MainView.Frame.Height), camera);
            map.MapType = MapViewType.Normal;
            map.Settings.ZoomGestures = false;
            map.Settings.CompassButton = false;
            map.Settings.MyLocationButton = false;
            map.MyLocationEnabled = false;
            map.SetMinMaxZoom(0, 0);
            MainView.Add(map);

            for (int c = 0; c < MarkerInfoDictionary.Count; c++)
            {
                MarkerInfo mi = MarkerInfoDictionary.ElementAt(c).Value;

                var Marker = new Marker();
                Marker.Position = new CLLocationCoordinate2D(mi.Lat, mi.Lon);
                Marker.Map = map;
                Marker.Title = mi.Title;
                Marker.Snippet = mi.Period +"\nClicca qua per richiedere il preventivo";

                //int HueGiallo = 49, HueViola = 290, HueRosso = 352, HueVerde = 141, HueArancione = 19, HueBlu = 228;
                switch(mi.IconHue)
                {
                    case 49:
                        Marker.Icon = Marker.MarkerImage(Giallo);
                        break;
                    case 290:
                        Marker.Icon = Marker.MarkerImage(Viola);
                        break;
                    case 352:
                        Marker.Icon = Marker.MarkerImage(Rosso);
                        break;
                    case 141:
                        Marker.Icon = Marker.MarkerImage(Verde);
                        break;
                    case 19:
                        Marker.Icon = Marker.MarkerImage(Arancione);
                        break;
                    case 228:
                        Marker.Icon = Marker.MarkerImage(Blu);
                        break;
                }



                MarkerList.Add(Marker);


            }
            map.InfoTapped += MapOnInfoWindowClick;

            map.MarkerInfoWindow = new GMSInfoFor(markerInfoWindow); //these above line of code will allow us to load a custom Marker Window instead of the default one

            UIView markerInfoWindow(UIView view, Marker marker)
            {
                // use this method to return the custom view u have already created to load as a subview in Google Map as Custom Marker Info Windo
                UIView v;

                v = new UIView();
                v.BackgroundColor = (UIColor.White);

                string MTitletxt = marker.Title;
                var MTitleSize = UIStringDrawing.StringSize(MTitletxt, UIFont.FromName("OpenSans-Bold", 16), new CGSize(View.Frame.Width - 30, 80));

                UILabel MTitleLabel = new UILabel();
                MTitleLabel.TextColor = UIColor.Black;
                MTitleLabel.TextAlignment = UITextAlignment.Center;
                MTitleLabel.Font = UIFont.FromName("OpenSans-Bold", 16);
                MTitleLabel.Lines = 0;
                MTitleLabel.Text = MTitletxt;
                v.Add(MTitleLabel);

                string MSnippettxt = marker.Snippet;
                var MSnippetSize = UIStringDrawing.StringSize(MSnippettxt, UIFont.FromName("OpenSans", 14), new CGSize(View.Frame.Width - 30, 80));

                UILabel MSnippetLabel = new UILabel();
                MSnippetLabel.TextColor = UIColor.Black;
                MSnippetLabel.TextAlignment = UITextAlignment.Center;
                MSnippetLabel.Font = UIFont.FromName("OpenSans", 14);
                MSnippetLabel.Lines = 0;
                MSnippetLabel.Text = MSnippettxt;
                v.Add(MSnippetLabel);


                nfloat vW = 0;
                nfloat vH = 30 + TitleSize.Height + MSnippetSize.Height;

                if (MSnippetSize.Width >= TitleSize.Width)
                    vW = MSnippetSize.Width + 20;
                else
                    vW = TitleSize.Width + 20;

                nfloat vX = view.Frame.Width / 2 - vW / 2;
                nfloat vY = view.Frame.Y + view.Frame.Height - vH;

                v.Frame = new CGRect(vX, vY, vW, vH);
                MTitleLabel.Frame = new CGRect(vW / 2 - MTitleSize.Width / 2, 10, MTitleSize.Width, TitleSize.Height);
                MSnippetLabel.Frame = new CGRect(vW / 2 - MSnippetSize.Width / 2, 20 + TitleSize.Height, MSnippetSize.Width, MSnippetSize.Height);    

                return v;
            }



            // MODAL

            ModalBackground = new UIView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height));
            ModalBackground.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 125);
            ModalBackground.Alpha = 0;

            UIView Modal = new UIView();
            Modal.BackgroundColor = UIColor.White;
            UITapGestureRecognizer ModalTap = new UITapGestureRecognizer(() => {
                //ModalBackground.Alpha = 0;
                View.EndEditing(true);
            });
            Modal.AddGestureRecognizer(ModalTap);
            Modal.UserInteractionEnabled = true;
            ModalBackground.Add(Modal);

            int ModalH = 0;

            string Daltxt = NSBundle.MainBundle.LocalizedString("ViaggioIdeale_Da", "", null);
            var DalSize = UIStringDrawing.StringSize(Daltxt, UIFont.FromName("OpenSans", 14), new CGSize(2000, 30));

            string Altxt = NSBundle.MainBundle.LocalizedString("ViaggioIdeale_A", "", null);
            var AlSize = UIStringDrawing.StringSize(Daltxt, UIFont.FromName("OpenSans", 14), new CGSize(2000, 30));

            UILabel DalLabel = new UILabel(new CGRect(10, 10 + 15-DalSize.Height/2, DalSize.Width, DalSize.Height));
            DalLabel.TextColor = UIColor.Black;
            DalLabel.Font = UIFont.FromName("OpenSans", 14);
            DalLabel.Lines = 1;
            DalLabel.Text = Daltxt;
            Modal.Add(DalLabel);

            var DataViewW = ((View.Frame.Width - 30) - AlSize.Width - DalSize.Width - 20 - 10 - 10)/2;

            UIView DalDataView = new UIView(new CGRect(DalLabel.Frame.X + DalLabel.Frame.Width + 5, 10, DataViewW, 30));
            DalDataView.BackgroundColor = Giallo;
            DalDataView.ClipsToBounds = true;
            DalDataView.Layer.CornerRadius = 5f;
            Modal.Add(DalDataView);


            UIView DalDataTxtBack = new UIView(new CGRect(1,1,DalDataView.Frame.Width -2 , DalDataView.Frame.Height-2));
            DalDataTxtBack.BackgroundColor = UIColor.FromRGB(237,237,237);
            DalDataTxtBack.Layer.CornerRadius = 5f;
            DalDataView.Add(DalDataTxtBack);

            UILabel DaDataTxt = new UILabel(new CGRect(5,5,DataViewW - 5 - 30 -5,20));
            DaDataTxt.TextColor = UIColor.Black;
            DaDataTxt.Font = UIFont.FromName("OpenSans-Bold", 13);
            DaDataTxt.Lines = 1;
            DalDataTxtBack.Add(DaDataTxt);

            UIView DalDataImageBack = new UIView(new CGRect(DalDataView.Frame.Width - DalDataView.Frame.Height, 0, DalDataView.Frame.Height, DalDataView.Frame.Height));
            DalDataImageBack.BackgroundColor = Giallo;
            DalDataView.Add(DalDataImageBack);

            UIImageView DalDataImage = new UIImageView(new CGRect(4,4,20,20));
            DalDataImage.Image = UIImage.FromFile("CalendarIcon.png");
            DalDataImageBack.Add(DalDataImage);

            UILabel AlLabel = new UILabel(new CGRect(DalDataView.Frame.X + DalDataView.Frame.Width +10, 10 + 15 - DalSize.Height / 2, DalSize.Width, DalSize.Height));
            AlLabel.TextColor = UIColor.Black;
            AlLabel.Font = UIFont.FromName("OpenSans", 14);
            AlLabel.Lines = 1;
            AlLabel.Text = Altxt;
            Modal.Add(AlLabel);

            UIView AlDataView = new UIView(new CGRect(AlLabel.Frame.X + AlLabel.Frame.Width + 5, 10, DataViewW, 30));
            AlDataView.BackgroundColor = Giallo;
            AlDataView.ClipsToBounds = true;
            AlDataView.Layer.CornerRadius = 5f;
            Modal.Add(AlDataView);

            UIView AlDataTxtBack = new UIView(new CGRect(1, 1, AlDataView.Frame.Width - 2, AlDataView.Frame.Height - 2));
            AlDataTxtBack.BackgroundColor = UIColor.FromRGB(237, 237, 237);
            AlDataTxtBack.Layer.CornerRadius = 5f;
            AlDataView.Add(AlDataTxtBack);

            UILabel ADataTxt = new UILabel(new CGRect(5, 5, DataViewW - 5 - 30 - 5, 20));
            ADataTxt.TextColor = UIColor.Black;
            ADataTxt.Font = UIFont.FromName("OpenSans-Bold", 13);
            ADataTxt.Lines = 1;
            AlDataTxtBack.Add(ADataTxt);

            UIView AlDataImageBack = new UIView(new CGRect(AlDataView.Frame.Width - AlDataView.Frame.Height , 0, AlDataView.Frame.Height, AlDataView.Frame.Height));
            AlDataImageBack.BackgroundColor = Giallo;
            AlDataView.Add(AlDataImageBack);

            UIImageView AlDataImage = new UIImageView(new CGRect(4, 4, 20, 20));
            AlDataImage.Image = UIImage.FromFile("CalendarIcon.png");
            AlDataImageBack.Add(AlDataImage);


            DateTime now = DateTime.Now;

            DaDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            ADate = DaDate.AddDays(1);

            DaDataTxt.Text = FormatDateTime(DaDate);
            ADataTxt.Text = FormatDateTime(ADate);

            ModalH += 40;

            string Nometxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_NomeTit", "", null);
            var NomeSize = UIStringDrawing.StringSize(Nometxt, UIFont.FromName("OpenSans", 14), new CGSize(2000, 30));

            UILabel NomeLabel = new UILabel(new CGRect(10, ModalH + 20, NomeSize.Width, NomeSize.Height));
            NomeLabel.TextColor = UIColor.Black;
            NomeLabel.Font = UIFont.FromName("OpenSans", 14);
            NomeLabel.Lines = 1;
            NomeLabel.Text = Nometxt;
            Modal.Add(NomeLabel);

            var PHCAttr = new UIStringAttributes { Font = UIFont.FromName("OpenSans", 14),ForegroundColor = UIColor.FromRGB(180, 180, 180) };

            var phcNomeTxt = new NSMutableAttributedString(Nometxt);
            phcNomeTxt.SetAttributes(PHCAttr.Dictionary, new NSRange(0, Nometxt.Length));

            UITextField NomeText = new UITextField(new CGRect(10 + NomeSize.Width + 10, NomeLabel.Frame.Y,  View.Frame.Width - 30 - NomeSize.Width -20, NomeSize.Height));
            NomeText.AttributedPlaceholder = phcNomeTxt;
            NomeText.TextColor = UIColor.Black;
            NomeText.Font = UIFont.FromName("OpenSans", 14);
            NomeText.KeyboardType = UIKeyboardType.Default;
            Modal.Add(NomeText);

            UIView NomeSep = new UITextField(new CGRect(10 + NomeSize.Width + 10, NomeLabel.Frame.Y + NomeSize.Height, View.Frame.Width - 30 - NomeSize.Width - 20, 1));
            NomeSep.BackgroundColor = Giallo;
            Modal.Add(NomeSep);

            ModalH += 21 + (int)NomeSize.Height;


            string Cognmetxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_CognomeTit", "", null);
            var CognmeSize = UIStringDrawing.StringSize(Cognmetxt, UIFont.FromName("OpenSans", 14), new CGSize(2000, 30));

            UILabel CognmeLabel = new UILabel(new CGRect(10, ModalH + 20, CognmeSize.Width, CognmeSize.Height));
            CognmeLabel.TextColor = UIColor.Black;
            CognmeLabel.Font = UIFont.FromName("OpenSans", 14);
            CognmeLabel.Lines = 1;
            CognmeLabel.Text = Cognmetxt;
            Modal.Add(CognmeLabel);

            var phcCognmeTxt = new NSMutableAttributedString(Cognmetxt);
            phcCognmeTxt.SetAttributes(PHCAttr.Dictionary, new NSRange(0, Cognmetxt.Length));

            UITextField CognmeText = new UITextField(new CGRect(10 + CognmeSize.Width + 10, CognmeLabel.Frame.Y, View.Frame.Width - 30 - CognmeSize.Width - 20, CognmeSize.Height));
            CognmeText.AttributedPlaceholder = phcCognmeTxt;
            CognmeText.TextColor = UIColor.Black;
            CognmeText.Font = UIFont.FromName("OpenSans", 14);
            CognmeText.KeyboardType = UIKeyboardType.Default;
            Modal.Add(CognmeText);

            UIView CognmeSep = new UITextField(new CGRect(10 + CognmeSize.Width + 10, CognmeLabel.Frame.Y + CognmeSize.Height, View.Frame.Width - 30 - CognmeSize.Width - 20, 1));
            CognmeSep.BackgroundColor = Giallo;
            Modal.Add(CognmeSep);

            ModalH += 21 + (int)CognmeSize.Height;

            string Emailtxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_EMailTit", "", null);
            var EmailSize = UIStringDrawing.StringSize(Emailtxt, UIFont.FromName("OpenSans", 14), new CGSize(2000, 30));

            UILabel EmailLabel = new UILabel(new CGRect(10, ModalH + 20, EmailSize.Width, EmailSize.Height));
            EmailLabel.TextColor = UIColor.Black;
            EmailLabel.Font = UIFont.FromName("OpenSans", 14);
            EmailLabel.Lines = 1;
            EmailLabel.Text = Emailtxt;
            Modal.Add(EmailLabel);

            var phcEmailTxt = new NSMutableAttributedString(Emailtxt);
            phcEmailTxt.SetAttributes(PHCAttr.Dictionary, new NSRange(0, Emailtxt.Length));

            UITextField EmailText = new UITextField(new CGRect(10 + EmailSize.Width + 10, EmailLabel.Frame.Y, View.Frame.Width - 30 - EmailSize.Width - 20, EmailSize.Height));
            EmailText.AttributedPlaceholder = phcEmailTxt;
            EmailText.TextColor = UIColor.Black;
            EmailText.Font = UIFont.FromName("OpenSans", 14);
            EmailText.KeyboardType = UIKeyboardType.EmailAddress;
            Modal.Add(EmailText);

            UIView EmailSep = new UITextField(new CGRect(10 + EmailSize.Width + 10, EmailLabel.Frame.Y + EmailSize.Height, View.Frame.Width - 30 - EmailSize.Width - 20, 1));
            EmailSep.BackgroundColor = Giallo;
            Modal.Add(EmailSep);

            ModalH += 21 + (int)EmailSize.Height;

            string Telefonotxt = NSBundle.MainBundle.LocalizedString("IlTuoViaggio_TelTit", "", null);
            var TelefonoSize = UIStringDrawing.StringSize(Telefonotxt, UIFont.FromName("OpenSans", 14), new CGSize(2000, 30));

            UILabel TelefonoLabel = new UILabel(new CGRect(10, ModalH + 20, TelefonoSize.Width, TelefonoSize.Height));
            TelefonoLabel.TextColor = UIColor.Black;
            TelefonoLabel.Font = UIFont.FromName("OpenSans", 14);
            TelefonoLabel.Lines = 1;
            TelefonoLabel.Text = Telefonotxt;
            Modal.Add(TelefonoLabel);

            var phcTelefonoTxt = new NSMutableAttributedString(Telefonotxt);
            phcTelefonoTxt.SetAttributes(PHCAttr.Dictionary, new NSRange(0, Telefonotxt.Length));

            UITextField TelefonoText = new UITextField(new CGRect(10 + TelefonoSize.Width + 10, TelefonoLabel.Frame.Y, View.Frame.Width - 30 - TelefonoSize.Width - 20, TelefonoSize.Height));
            TelefonoText.AttributedPlaceholder = phcTelefonoTxt;
            TelefonoText.TextColor = UIColor.Black;
            TelefonoText.Font = UIFont.FromName("OpenSans", 14);
            TelefonoText.KeyboardType = UIKeyboardType.NumberPad;
            Modal.Add(TelefonoText);

            UIView TelefonoSep = new UITextField(new CGRect(10 + TelefonoSize.Width + 10, TelefonoLabel.Frame.Y + TelefonoSize.Height, View.Frame.Width - 30 - TelefonoSize.Width - 20, 1));
            TelefonoSep.BackgroundColor = Giallo;
            Modal.Add(TelefonoSep);

            ModalH += 21 + (int)TelefonoSize.Height;

            ModalSendEnabled = true;
            UIView Button = new UIView(new CGRect((View.Frame.Width - 30)/2 - 100 ,ModalH +20,200,60));
            Button.BackgroundColor = Giallo;
            Button.Layer.CornerRadius = 5;
            UITapGestureRecognizer ButtonTap = new UITapGestureRecognizer(() => {
                View.EndEditing(true);
                if(ModalSendEnabled)
                {
                    APIViaggioIdeale(DaDataTxt.Text, ADataTxt.Text,
                                     CognmeText.Text, NomeText.Text, TelefonoText.Text, EmailText.Text,
                                     ModalMarkerTitle);
                }
            });
            Button.AddGestureRecognizer(ButtonTap);
            Button.UserInteractionEnabled = true;
            Modal.Add(Button);


            UIImageView ButtonImage = new UIImageView(new CGRect(15, 15, 30, 30));
            ButtonImage.Image = UIImage.FromFile("ViaggioIdealeBtnIcon.png");
            Button.Add(ButtonImage);


            string Buttontxt = NSBundle.MainBundle.LocalizedString("ViaggioIdeale_SendTxt", "", null);
            var ButtonSize = UIStringDrawing.StringSize(Buttontxt, UIFont.FromName("OpenSans", 14), new CGSize(130, 60));

            UILabel ButtonLabel = new UILabel(new CGRect(55, 30- ButtonSize.Height/2, ButtonSize.Width, ButtonSize.Height));
            ButtonLabel.TextColor = UIColor.White;
            ButtonLabel.TextAlignment = UITextAlignment.Center;
            ButtonLabel.Font = UIFont.FromName("OpenSans", 14);
            ButtonLabel.Lines = 0;
            ButtonLabel.Text = Buttontxt;
            Button.Add(ButtonLabel);

            var ButtonW = 15 + 30 + 10 + ButtonSize.Width + 15;
            Button.Frame = new CGRect((View.Frame.Width - 30) / 2 - ButtonW / 2, ModalH + 20, ButtonW , 60);

            ModalH += 80;

            Modal.Frame = new CGRect(15, ModalBackground.Frame.Height / 2 - ModalH / 2, View.Frame.Width - 30, ModalH+20);

            UIView PickerBack = new UIView(new CGRect(0, 0,Modal.Frame.Width,Modal.Frame.Height));
            PickerBack.BackgroundColor = UIColor.White;
            PickerBack.Alpha = 0;
            Modal.Add(PickerBack);

            UIDatePicker PickerView = new UIDatePicker()
            {
                Frame = new CGRect(20, (PickerBack.Frame.Height / 2) - 90, PickerBack.Frame.Width - 40, 180),
            };
            PickerView.Mode = UIDatePickerMode.Date;
            PickerView.BackgroundColor = UIColor.White;
            PickerView.MinimumDate = (NSDate)DateTime.Now;
            PickerBack.Add(PickerView);

            UIButton PickerOK = new UIButton(new CGRect(PickerBack.Frame.Width/2 - 30, PickerView.Frame.Y + 200, 60, 30));
            PickerOK.BackgroundColor = Giallo;
            PickerOK.SetTitle("OK", UIControlState.Normal);
            PickerOK.SetTitleColor(UIColor.White, UIControlState.Normal);
            PickerOK.TouchUpInside += delegate {

                NSCalendar cal = NSCalendar.CurrentCalendar;
                var y = (int)cal.GetComponentFromDate(NSCalendarUnit.Year, PickerView.Date);
                var m = (int)cal.GetComponentFromDate(NSCalendarUnit.Month, PickerView.Date);
                var d = (int)cal.GetComponentFromDate(NSCalendarUnit.Day, PickerView.Date);

                if (DaOpen)
                {
                    
                    DaDate = new DateTime(y, m, d, 0, 0, 0);
                    DaDataTxt.Text = FormatDateTime(DaDate);

                    if (ADate.CompareTo(DaDate) < 0)
                    {
                        ADate = DaDate.AddDays(1);
                        ADataTxt.Text = FormatDateTime(ADate);
                    }
                }

                if (AOpen)
                {
                    ADate = new DateTime(y, m , d, 0, 0, 0);
                    ADataTxt.Text = FormatDateTime(ADate);
                }


                DaOpen = false;
                AOpen = false;
                PickerBack.Alpha = 0;

            };
            PickerBack.Add(PickerOK);

            UITapGestureRecognizer DalDataTap = new UITapGestureRecognizer(() => {
                View.EndEditing(true);

                DaOpen = true;
                AOpen = false;

                PickerBack.Alpha = 1;

                PickerView.SetDate((NSDate)DateTime.SpecifyKind(DaDate, DateTimeKind.Local),false);
                //PickerView.MinimumDate = 0;
                PickerView.MinimumDate = (NSDate)DateTime.Now;

            });
            DalDataView.AddGestureRecognizer(DalDataTap);
            DalDataView.UserInteractionEnabled = true;   

            UITapGestureRecognizer AlDataTap = new UITapGestureRecognizer(() => {
                View.EndEditing(true);

                DaOpen = false;
                AOpen = true;

                PickerBack.Alpha = 1;

                PickerView.SetDate((NSDate)DateTime.SpecifyKind(ADate, DateTimeKind.Local), false);
                //PickerView.MinimumDate = 0;
                PickerView.MinimumDate = (NSDate)DateTime.SpecifyKind(DaDate.AddDays(1), DateTimeKind.Local);

            });
            AlDataView.AddGestureRecognizer(AlDataTap);
            AlDataView.UserInteractionEnabled = true;   

            UITapGestureRecognizer ModalBackTap = new UITapGestureRecognizer(() => {

                if(NomeText.IsEditing || CognmeText.IsEditing || EmailText.IsEditing || TelefonoText.IsEditing)
                    View.EndEditing(true);
                else
                    ModalBackground.Alpha = 0;
            });
            ModalBackground.AddGestureRecognizer(ModalBackTap);
            ModalBackground.UserInteractionEnabled = true;

            // FINE MODAL



            View.Add(NavigationBar);
            View.Add(TitleView);
            View.Add(MainView);
            View.Add(ModalBackground);

            // INIT MENU

            menu = new UIMenuView(new CGRect(0, 70, View.Frame.Width, View.Frame.Height - 70));
            menu.SetBackgroundColor(UIColor.FromRGBA(0, 0, 0, 75));
            menu.SetMenuColor(UIColor.FromRGB(102, 104, 162));
            menu.PAGE_TAG = "5";
            menu.CellHeight = 50;
            menu.CellWidht = 200;
            menu.MenuImg = MenuImage;

            menu.AddElement(GbsInstance.ListMenuElement);

            View.Add(menu);

            DismissKeyboardOnBackgroundTap();
        }

        private void MapOnInfoWindowClick(object sender, GMSMarkerEventEventArgs e)
        {
            Marker myMarker = e.Marker;
            Console.WriteLine("M Tit:"+ myMarker.Title);

            ModalMarkerTitle = myMarker.Title;

            ModalBackground.Alpha = 1;
            //ModalPreventivo.Visibility = ViewStates.Visible;

            /*
            MarkerInfo miCl = MarkerDictionary[myMarker.Id];
            if (miCl.nome == "PARCHEGGI")
            {
                Console.WriteLine("geo:" + miCl.lat + "," + miCl.lon);
                string lat = miCl.lat.ToString().Replace(",", ".");
                string lon = miCl.lon.ToString().Replace(",", ".");
                Console.WriteLine("geo:" + lat + "," + lon + "?q=" + WebUtility.UrlEncode(miCl.via));
                var geoUri = Android.Net.Uri.Parse("geo:" + miCl.lat + "," + miCl.lon + "?q=" + WebUtility.UrlEncode(miCl.via));
                var mapIntent = new Intent(Intent.ActionView, geoUri);
                StartActivity(mapIntent);
            }
            else
            {
                var intent = new Intent(this, typeof(CosaVedereInfoActivity));
                intent.PutExtra("key", myMarker.Id);
                StartActivity(intent);
            }

            */
        }

        public void DismissKeyboardOnBackgroundTap()
        {
            // Add gesture recognizer to hide keyboard
            var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
            tap.AddTarget(() => View.EndEditing(true));
            View.AddGestureRecognizer(tap);
        }

        public void APIViaggioIdeale(string data_partenza, string data_fine,string cognome,string nome,string telefono, string email, string localita){

            ModalSendEnabled = false;
                      
            bool flagNome = false;
            bool flagEMail = false;
            //bool flagMessage = false;
            bool flagCognome = false;
            //bool flagTelefono = false;


            if (string.IsNullOrEmpty(nome))
            {
                flagNome = true;
            }
            if (string.IsNullOrEmpty(cognome))
            {
                flagCognome = true;
            }

            if (string.IsNullOrEmpty(email))
            {
                flagEMail = true;
            }
            else
            {
                try
                {
                    MailAddress m = new MailAddress(email);
                }
                catch (FormatException)
                {
                    flagEMail = true;
                }
            }


            if (flagEMail || flagNome  || flagCognome)
            {
                ModalSendEnabled = true;
                Toast.MakeToast("Errore dati non inseriti o errati").Show();
                return;

            }

            var client = new RestClient(GbsInstance.BaseUrl);

            var requestN4U = new RestRequest("mail_app.php", Method.POST);
            requestN4U.AddHeader("content-type", "application/json");
            //requestN4U.AddParameter("application/x-www-form-urlencoded", $"post={"3"}&email={"matteo.lomi@netwintec.com"}&nome={"Matteo"}&messaggio={"Test Messaggio"}", ParameterType.RequestBody);
            JObject oJsonObject = new JObject();

            oJsonObject.Add("tipo", "1");
            oJsonObject.Add("data_partenza", data_partenza);
            oJsonObject.Add("data_fine", data_fine);
            oJsonObject.Add("cognome", cognome);
            oJsonObject.Add("nome", nome);
            oJsonObject.Add("telefono", telefono);
            oJsonObject.Add("email", email);
            oJsonObject.Add("localita", localita);

            requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);
            Console.WriteLine(client.BaseUrl + "" + requestN4U.Resource);

            client.ExecuteAsync(requestN4U,(response, arg2) => {
                
                Console.WriteLine("StatusCode mail_app.php:" + response.StatusCode +
                                  "\nContent mail_app.php:" + response.Content);

                if(response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    InvokeOnMainThread(() =>
                    {

                        ModalSendEnabled = true;
                        ModalBackground.Alpha = 0;
                        Toast.MakeToast("Email inviata con successo").Show();

                    });
                }
                else
                {
                    InvokeOnMainThread(() =>
                    {

                        ModalSendEnabled = true;
                        ModalBackground.Alpha = 0;
                        Toast.MakeToast("Errore riprovare più tardi").Show();

                    });
                }

            });

           
        } 

        public string FormatDateTime(DateTime dt)
        {

            string formatted = "";

            formatted = dt.ToString("dd MMM yyyy");

            return formatted;

        }
         
    }
}