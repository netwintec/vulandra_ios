using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace Vulandra
{
    public partial class ChiSiamoPage : UIViewController
    {
        
        GlobalStructureInstance GbsInstance = GlobalStructureInstance.Instance;

        UIMenuView menu;

        string MainText =
            "Vulandra Viaggi rappresenta una agenzia viaggi controtendenza perché si attiene a una filosofia di vendita che mira a turismo per addizione anziché un turismo per sottrazione." +
            "\nNegli ultimi anni il mondo commerciale è stato inondato solo da forme di sconto diverse cosa che però ha portato a una grossa perdita di qualità." +
            "\n\nVulandra Viaggi vi offre i migliori servizi ma anche:";
            //+ "\n__BOLDTEXT__" +
        string MainText2 ="\n\nVulandra viaggi è l'agenzia che non ti aspetti dove il mio tempo è a tua disposizione per ascoltare le tue esperienze passate e le tue esigenze future con attenzione, rispetto e serietà." +
            "\n\nSolo attraverso la cura delle vostre necessità è possibile realizzare per ognuno di voi viaggi indimenticabili.\n\nTroverete le soluzioni più accurate per single, coppie, famiglie, gruppi di amici e per chi non vuole rinunciare alla compagnia degli inseparabili amici a quattro zampe.Un'attenzione particolare la rivolgiamo anche a occasioni speciali come anniversari, compleanni, lauree, ricorrenze e viaggi di nozze in collaborazione con i migliori professionisti nel campo dei servizi fotografici e dei trattamenti benessere.";

        string MainBoldText =
            "- Più competenza e professionalità" +
            "\n- Più esperienza e attenzione" +
            "\n- Più specializzazione" +
            "\n- Più scelta" +
            "\n- Più regali e gadget" +
            "\n- Più eventi" +
            "\n- Consulenza in LIS tramite videocall per sordi";


        public static ChiSiamoPage Instance { private set; get; }

        public ChiSiamoPage(IntPtr handle) : base(handle)
        {
        }


        public override void ViewDidAppear(bool animated)
        {
            try
            {
                GbsInstance.SetPage(PageName.ChiSiamo);
            }
            catch (Exception) { };

            base.ViewDidAppear(animated);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            GbsInstance.SetPage(PageName.ChiSiamo);

            ChiSiamoPage.Instance = this;

            UIView NavigationBar = new UIView(new CGRect(0, 0, View.Frame.Width, 70));
            NavigationBar.BackgroundColor = UIColor.FromRGB(102, 104, 162);

            UIImageView MenuImage = new UIImageView(new CGRect(15, 27.5, 35, 35));
            MenuImage.Image = UIImage.FromFile("MenuClose.png");
            var imTap = new UITapGestureRecognizer(() => {
                if (menu.MenuOpen)
                {
                    menu.CloseMenu();
                }
                else
                {
                    menu.OpenMenu();
                }
            });
            MenuImage.UserInteractionEnabled = true;
            MenuImage.AddGestureRecognizer(imTap);
            NavigationBar.Add(MenuImage);

            UIImageView BackImage = new UIImageView(new CGRect(View.Frame.Width - 50, 27.5, 35, 35));
            BackImage.Image = UIImage.FromFile("Back.png");
            var backTap = new UITapGestureRecognizer(() => {
                this.NavigationController.PopToRootViewController(true);
            });
            BackImage.UserInteractionEnabled = true;
            BackImage.AddGestureRecognizer(backTap);
            NavigationBar.Add(BackImage);

            UIImageView LogoImage = new UIImageView(new CGRect(View.Frame.Width / 2 - 52.5, 27.5, 105, 35));
            LogoImage.Image = UIImage.FromFile("Logo.png");
            NavigationBar.Add(LogoImage);

            UIView Sep = new UIView(new CGRect(0, 68, View.Frame.Width, 2));
            Sep.BackgroundColor = UIColor.FromRGB(226, 105, 39);
            NavigationBar.Add(Sep);

            UIView TitleView = new UIView(new CGRect(0, 70, View.Frame.Width, 60));
			TitleView.BackgroundColor = UIColor.FromRGB(226, 105, 39);

            string Titletxt = NSBundle.MainBundle.LocalizedString("ChiSiamo_Tit", "", null);
            var TitleSize = UIStringDrawing.StringSize(Titletxt, UIFont.FromName("OpenSans-Bold", 18), new CGSize(View.Frame.Width - 30, 80));

            UILabel TitleLabel = new UILabel(new CGRect(15, TitleView.Frame.Height/2 - TitleSize.Height/2, View.Frame.Width - 30, TitleSize.Height));
            TitleLabel.TextColor = UIColor.White;
            TitleLabel.TextAlignment = UITextAlignment.Center;
            TitleLabel.Font = UIFont.FromName("OpenSans-Bold", 18);
            TitleLabel.Lines = 0;
            TitleLabel.Text = Titletxt;
            TitleView.Add(TitleLabel);

            UIView SepTitle = new UIView(new CGRect(0, 58, View.Frame.Width, 2));
            SepTitle.BackgroundColor = UIColor.FromRGB(226, 105, 39);
            TitleView.Add(SepTitle);

			UIView MainView = new UIView(new CGRect(0, 130, View.Frame.Width, View.Frame.Height - 130));
            MainView.BackgroundColor = UIColor.White;

            UIScrollView sc = new UIScrollView(new CGRect(0, 0, MainView.Frame.Width, MainView.Frame.Height));

            nfloat yy = 15;
            var PrSize = UIStringDrawing.StringSize(MainText, UIFont.FromName("OpenSans",16), new CGSize(View.Frame.Width - 30, 20000));

            // Serve font monserratbold
            UILabel MainTextLabel = new UILabel(new CGRect(15, yy, View.Frame.Width - 30, PrSize.Height));
            MainTextLabel.TextColor = UIColor.Black;
            MainTextLabel.Font = UIFont.FromName("OpenSans",16);
            MainTextLabel.Lines = 0;
            MainTextLabel.Text = MainText;
            sc.Add(MainTextLabel);
            yy += PrSize.Height;

            var PrSizeBold = UIStringDrawing.StringSize(MainBoldText, UIFont.FromName("OpenSans-Bold",16), new CGSize(View.Frame.Width - 30, 20000));

            // Serve font monserratbold
            UILabel MainTextBoldLabel = new UILabel(new CGRect(15, yy, View.Frame.Width - 30, PrSizeBold.Height));
            MainTextBoldLabel.TextColor = UIColor.Black;
            MainTextBoldLabel.Lines = 0;
            MainTextBoldLabel.Font = UIFont.FromName("OpenSans-Bold",16);
            MainTextBoldLabel.Text = MainBoldText;
            sc.Add(MainTextBoldLabel);
            yy += PrSizeBold.Height;

            var PrSize2 = UIStringDrawing.StringSize(MainText2, UIFont.FromName("OpenSans",16), new CGSize(View.Frame.Width - 30, 20000));

            // Serve font monserratbold
            UILabel MainText2Label = new UILabel(new CGRect(15, yy, View.Frame.Width - 30, PrSize2.Height));
            MainText2Label.TextColor = UIColor.Black;
            MainText2Label.Lines = 0;
            MainText2Label.Font = UIFont.FromName("OpenSans",16);
            MainText2Label.Text = MainText2;
            sc.Add(MainText2Label);
            yy += PrSize2.Height;

            sc.ContentSize = (new CGSize(View.Frame.Width, yy + 15));

            MainView.Add(sc);
            View.Add(NavigationBar);
            View.Add(TitleView);
            View.Add(MainView);

            // INIT MENU

            menu = new UIMenuView(new CGRect(0, 70, View.Frame.Width, View.Frame.Height - 70));
            menu.SetBackgroundColor(UIColor.FromRGBA(0, 0, 0, 75));
            menu.SetMenuColor(UIColor.FromRGB(102, 104, 162));
            menu.PAGE_TAG = "5";
            menu.CellHeight = 50;
            menu.CellWidht = 200;
            menu.MenuImg = MenuImage;

            menu.AddElement(GbsInstance.ListMenuElement);

            View.Add(menu);
        }

    }

}