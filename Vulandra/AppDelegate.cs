﻿using System;
using System.Text;
using Foundation;
using Google.Maps;
using RestSharp;
using UIKit;

namespace Vulandra
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        // class-level declarations

        string MapKey = "AIzaSyCRRYN0uJOY4jUmditVGAzgHnPcyKN3kcM";

        GlobalStructureInstance GbsInstance;
        public override UIWindow Window
        {
            get;
            set;
        }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            // Override point for customization after application launch.
            // If not required for your application you can safely delete this method


            /****** APP FONT ******

            Montserrat-Bold
            OpenSans-SemiBold
            OpenSans
            OpenSans-Bold

            **********************/

            /*
            var fontList = new StringBuilder();
            var familyNames = UIFont.FamilyNames;

            foreach (var familyName in familyNames)
            {
                fontList.Append(String.Format("Family; {0} \n", familyName));
                Console.WriteLine("Family: {0}\n", familyName);

                var fontNames = UIFont.FontNamesForFamilyName(familyName);

                foreach (var fontName in fontNames)
                {
                    Console.WriteLine("\tFont: {0}\n", fontName);
                }

                // Perform any additional setup after loading the view, typically from a nib.
            }*/

            MapServices.ProvideAPIKey(MapKey);

            GbsInstance = new GlobalStructureInstance();


            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                    new NSSet());
                UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }

            // create a new window instance based on the screen size
            Window = new UIWindow(UIScreen.MainScreen.Bounds);

            // make the window visible 
            Window.MakeKeyAndVisible();

            var storyboard = UIStoryboard.FromName("Main", null);
            UIViewController lp = storyboard.InstantiateViewController("Home");
            var navController = new UINavigationController(lp);
            Window.RootViewController = navController;

			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
			application.ApplicationIconBadgeNumber = 0;
			             
            return true;
        }

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            //Get current device token
            var DeviceToken = deviceToken.Description;
            Console.WriteLine("PUSH TOKEN1:" + DeviceToken);
            if (!string.IsNullOrWhiteSpace(DeviceToken))
            {
                DeviceToken = DeviceToken.Replace("<", "").Replace(">", "").Replace(" ", "");
            }
           
            SendToken(DeviceToken, 0);

        }
        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            Console.WriteLine("PUSH TOKEN ERROR:" + error.LocalizedDescription);

            //new UIAlertView("Error registering push notifications", error.LocalizedDescription, null, "OK", null).Show();
            //GlobalStructureInstance.Instance.TokenError = "Error:" + error.LocalizedDescription;
        }

        public void SendToken(string token, int time)
        {
            var client = new RestClient(GbsInstance.BaseUrl);

			var request = new RestRequest("wp-json/apnwp/register?os_type=ios&device_token="+token+"&user_email_id=" + UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString(), Method.GET);

            Console.WriteLine(client.BaseUrl + request.Resource);

            client.ExecuteAsync(request, (sender, e) =>
            {

                IRestResponse response = sender;

                InvokeOnMainThread(() =>
                {
                    Console.WriteLine(response.Server + "\nStatusCode:" + response.StatusCode + "\nContent:" + response.Content);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                    }
                    else
                    {

                        if (time <= 5)
                            SendToken(token, time + 1);

                    }
                });
            });
        

        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            //This method gets called whenever the app is already running and receives a push notification
            // YOU MUST HANDLE the notifications in this case.  Apple assumes if the app is running, it takes care of everything
            // this includes setting the badge, playing a sound, etc.
            Console.WriteLine("remote" + application.ApplicationState.ToString());

            foreach (var obj in userInfo)
            {
                Console.WriteLine(obj.Key + ":" + obj.Value);
            }

        }
    }
}