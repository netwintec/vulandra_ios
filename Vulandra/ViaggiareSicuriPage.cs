using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace Vulandra
{
    public partial class ViaggiareSicuriPage : UIViewController
    {
     
        GlobalStructureInstance GbsInstance = GlobalStructureInstance.Instance;

        UIMenuView menu;

        public static ViaggiareSicuriPage Instance { private set; get; }

        public ViaggiareSicuriPage(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidAppear(bool animated)
        {
            try
            {
                GbsInstance.SetPage(PageName.ViaggiareSicuri);
            }
            catch (Exception) { };

            base.ViewDidAppear(animated);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            GbsInstance.SetPage(PageName.ViaggiareSicuri);

            ViaggiareSicuriPage.Instance = this;

            UIView NavigationBar = new UIView(new CGRect(0, 0, View.Frame.Width, 70));
            NavigationBar.BackgroundColor = UIColor.FromRGB(102, 104, 162);

            UIImageView MenuImage = new UIImageView(new CGRect(15, 27.5, 35, 35));
            MenuImage.Image = UIImage.FromFile("MenuClose.png");
            var imTap = new UITapGestureRecognizer(() => {
                if (menu.MenuOpen)
                {
                    menu.CloseMenu(); 
                }
                else
                {
                    menu.OpenMenu();
                }
            });
            MenuImage.UserInteractionEnabled = true;
            MenuImage.AddGestureRecognizer(imTap);
            NavigationBar.Add(MenuImage);

            UIImageView BackImage = new UIImageView(new CGRect(View.Frame.Width - 50, 27.5, 35, 35));
            BackImage.Image = UIImage.FromFile("Back.png");
            var backTap = new UITapGestureRecognizer(() => {
                this.NavigationController.PopToRootViewController(true);
            });
            BackImage.UserInteractionEnabled = true;
            BackImage.AddGestureRecognizer(backTap);
            NavigationBar.Add(BackImage);

            UIImageView LogoImage = new UIImageView(new CGRect(View.Frame.Width / 2 - 52.5, 27.5, 105, 35));
            LogoImage.Image = UIImage.FromFile("Logo.png");
            NavigationBar.Add(LogoImage);

            UIView Sep = new UIView(new CGRect(0, 68, View.Frame.Width, 2));
            Sep.BackgroundColor = UIColor.FromRGB(44, 169, 91);
            NavigationBar.Add(Sep);

            UIView MainView = new UIView(new CGRect(0, 70, View.Frame.Width, View.Frame.Height - 70));
            MainView.BackgroundColor = UIColor.White;

            UIWebView web = new UIWebView(new CGRect(0, 0, MainView.Frame.Width, MainView.Frame.Height));
            web.Alpha = 0;
            var url = "http://www.viaggiaresicuri.it/home/"; // NOTE: https secure request
            web.LoadRequest(new NSUrlRequest(new NSUrl(url)));
            web.LoadFinished += delegate {

                web.Alpha = 1;

            };

            UIActivityIndicatorView loader  = new UIActivityIndicatorView (new CGRect(MainView.Frame.Width/2 -10, MainView.Frame.Height/2 -10 ,20 ,20));
            loader.StartAnimating();
            loader.Color = UIColor.FromRGB(44, 169, 91);

            MainView.Add(loader);
            MainView.Add(web);



            View.Add(NavigationBar);
            View.Add(MainView);

            // INIT MENU

            menu = new UIMenuView(new CGRect(0, 70, View.Frame.Width, View.Frame.Height - 70));
            menu.SetBackgroundColor(UIColor.FromRGBA(0, 0, 0, 75));
            menu.SetMenuColor(UIColor.FromRGB(102, 104, 162));
            menu.PAGE_TAG = "5";
            menu.CellHeight = 50;
            menu.CellWidht = 200;
            menu.MenuImg = MenuImage;

            menu.AddElement(GbsInstance.ListMenuElement);

            View.Add(menu);
        }

    }
}